#pragma once
#include "Node.h"

class ProgramElements : public Node
{
private:
	Node * _programElem = nullptr;
	Node * _programElems = nullptr;

	std::list<Node*> inner;
public:
	ProgramElements();
	ProgramElements(std::list<Token> tkns, std::list<Node*> nodes);
	~ProgramElements();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();	
};

