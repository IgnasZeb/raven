#include "AddingExpression.h"
#include "../../Semantic Analyser/SemanticErrors.h"

AddingExpression::AddingExpression()
{
}

AddingExpression::AddingExpression(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 1)
		_inner = nodes.front();
	else
	{
		_left = nodes.front();
		nodes.pop_front();
		_innerOpNode = nodes.front();
		_right = nodes.back();

		_op = _innerOpNode->getToken();
	}
}


AddingExpression::~AddingExpression()
{
}

NodeTypes::NodeType AddingExpression::getNodeType()
{
	return NodeTypes::AddingExpression;
}

Node * AddingExpression::get()
{
	if (_inner == nullptr)
		return this;
	else
		return _inner->get();
}

std::string AddingExpression::getName()
{
	return "AddingExpression";
}

void AddingExpression::PrintTree(int indentation)
{
	if (_inner == nullptr)
	{
		_printLine(indentation, "AddingExpression");
		_printLine(indentation + 1, "_left:");
		_left->get()->PrintTree(indentation+2);
		_printLine(indentation + 1, "_right");
		_right->get()->PrintTree(indentation + 2);
		_printLine(indentation + 1, "_op");
		_printLine(indentation + 1, "Token", _op.GetName());
	}
	else
	{
		_inner->get()->PrintTree(indentation+1);
	}
}

void AddingExpression::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	if (_inner != nullptr)
		_inner->FillSymbolTable(symbols);
	else
	{
		_left->FillSymbolTable(symbols);
		_right->FillSymbolTable(symbols);
	}
}

void AddingExpression::ResolveNames()
{
	if (_inner == nullptr)
	{
		_left->ResolveNames();
		_right->ResolveNames();
	}
	else
		_inner->ResolveNames();
}

void AddingExpression::TestTypes()
{
	if (_inner == nullptr)
	{
		_left->TestTypes();
		_right->TestTypes();
		if (_left->get()->getSymbolType() != _right->get()->getSymbolType())
			SemanticErrors::AddError(SemanticErrors::Error(5, getLineNo(), { Symbol::GetTypeName(_left->getSymbolType()), Symbol::GetTypeName(_right->getSymbolType()) }));
	}
	else
		_inner->TestTypes();
}

Symbol::Type AddingExpression::getSymbolType()
{
	if (_inner != nullptr)
		return _inner->get()->getSymbolType();
	else
		return _left->get()->getSymbolType();
}

int AddingExpression::getLineNo()
{
	if (_inner != nullptr)
		return _inner->getLineNo();
	else
		return _op.line;
}
