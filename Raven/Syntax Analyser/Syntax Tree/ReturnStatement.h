#pragma once
#include "Node.h"

class ReturnStatement : public Node
{
public:
	Node * _right;
public:
	ReturnStatement();
	ReturnStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~ReturnStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	Node * getInner();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};