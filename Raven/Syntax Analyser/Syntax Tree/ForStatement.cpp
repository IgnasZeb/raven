#include "ForStatement.h"
#include "../../Semantic Analyser/SemanticErrors.h"


ForStatement::ForStatement()
{
}

ForStatement::ForStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_from = nodes.front();
	nodes.pop_front();
	_to = nodes.front();
	nodes.pop_front();
	_stmtBlock = nodes.back();
	
	if (nodes.size() == 2)
		_step = nodes.front();
	else if (nodes.size() != 1)
		throw "Invalid node list size!";

	tkns.pop_back();
	_as = tkns.back();
}

ForStatement::~ForStatement()
{
}

NodeTypes::NodeType ForStatement::getNodeType()
{
	return NodeTypes::ForStatement;
}

Node * ForStatement::get()
{
	return this;
}

Node * ForStatement::getInner()
{
	return _stmtBlock;
}

std::string ForStatement::getName()
{
	return "ForStatement";
}

int ForStatement::getLineNo()
{
	return _as.line;
}

void ForStatement::PrintTree(int indentation)
{
	_printLine(indentation, "ForStatement");
	_printLine(indentation + 1, "_from");
	_from->get()->PrintTree(indentation + 2);
	_printLine(indentation + 1, "_to");
	_to->get()->PrintTree(indentation + 2);
	if (_step != nullptr)
	{
		_printLine(indentation + 1, "_step");
		_step->get()->PrintTree(indentation + 2);
	}

	_printLine(indentation + 1, "_as");
	_printLine(indentation + 2, "Token", _as.GetName());
	_stmtBlock->get()->PrintTree(indentation+1);
}

void ForStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	SymbolTable & forScope = symbols.AddScope();
	forScope.AddSymbol(Symbol(Symbol::SymbolType::Variable, Symbol::Type::Int, _as));
	_stmtBlock->get()->FillSymbolTable(forScope, true);
}

void ForStatement::ResolveNames()
{
	_from->ResolveNames();
	_to->ResolveNames();
	if (_step != nullptr)
		_step->ResolveNames();
	_stmtBlock->ResolveNames();
}

void ForStatement::TestTypes()
{
	_from->TestTypes();
	_to->TestTypes();
	if (_step != nullptr)
		_step->TestTypes();
	_stmtBlock->TestTypes();

	if (_from->get()->getSymbolType() != KWD_INT || _to->get()->getSymbolType() != KWD_INT || (_step != nullptr && _step->getSymbolType() != KWD_INT))
		SemanticErrors::AddError(SemanticErrors::Error(4, _as.line, {}));
}
