#pragma once
#include "Node.h"

class Type : public Node
{
private:
	Token _type;
public:
	Type();
	Type(std::list<Token> tkns, std::list<Node*> nodes);
	~Type();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	Token getToken();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);
};