#include "FunctionDefinition.h"

FunctionDefinition::FunctionDefinition()
{
}

FunctionDefinition::FunctionDefinition(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() == 0)
		throw "Invalid token list size!";

	_ident = tkns.front();

	_type = nodes.front();
	_statementBlock = nodes.back();

	if (nodes.size() == 3)
	{
		_parameters = (*++(nodes.begin()));
	}
	else if (nodes.size() == 2)
		_parameters = nullptr;
	else
		throw "Invalid node list size!";
}

FunctionDefinition::~FunctionDefinition()
{
}

NodeTypes::NodeType FunctionDefinition::getNodeType()
{
	return NodeTypes::FunctionDefinition;
}

Node * FunctionDefinition::get()
{
	return this;
}

Node * FunctionDefinition::getInner()
{
	return _type;
}

std::list<Node*> FunctionDefinition::getInnerList()
{
	std::list<Node*> retList;
	for (auto p : _parameters->getInnerList())
		retList.push_back(p);

	return retList;
}

Token FunctionDefinition::getToken()
{
	return _ident;
}

void FunctionDefinition::PrintTree(int indentation)
{
	_printLine(indentation, "FunctionDefinition");
	_printLine(indentation + 1, "_ident");
	_printLine(indentation + 2, "Token", _ident.value.GetStringValue());
	_printLine(indentation + 1, "_type");
	_type->get()->PrintTree(indentation + 2);
	if (_parameters != nullptr)
	{
		_printLine(indentation + 1, "_parameters");
		_parameters->get()->PrintTree(indentation + 2);
	}
	_printLine(indentation + 1, "_statementBlock");
	_statementBlock->get()->PrintTree(indentation + 2);
}

void FunctionDefinition::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	symbols.AddSymbol(Symbol(Symbol::SymbolType::Function, _type->getSymbolType(), _ident), (void*)this);

	SymbolTable & functionScope = symbols.AddScope();
	if(_parameters != nullptr)
		_parameters->get()->FillSymbolTable(functionScope);
	_statementBlock->get()->FillSymbolTable(functionScope, true);
}

void FunctionDefinition::ResolveNames()
{
	_statementBlock->ResolveNames();
}

void FunctionDefinition::TestTypes()
{
	_statementBlock->TestTypes();
}

Symbol::Type FunctionDefinition::getSymbolType()
{
	return _type->getSymbolType();
}

int FunctionDefinition::getLineNo()
{
	return _ident.line;
}
