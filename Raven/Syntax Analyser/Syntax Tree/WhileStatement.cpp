#include "WhileStatement.h"

#include "../../Semantic Analyser/SemanticErrors.h"
WhileStatement::WhileStatement()
{
}

WhileStatement::WhileStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 2)
		throw "Invalid node list size!";

	_whileExpr = nodes.front();
	_stmtBlock = nodes.back();
}

WhileStatement::~WhileStatement()
{
}

NodeTypes::NodeType WhileStatement::getNodeType()
{
	return NodeTypes::WhileStatement;
}

Node * WhileStatement::get()
{
	return this;
}

Node * WhileStatement::getInner()
{
	return _stmtBlock;
}

std::string WhileStatement::getName()
{
	return "WhileStatement";
}

int WhileStatement::getLineNo()
{
	return _whileExpr->getLineNo();
}

void WhileStatement::PrintTree(int indentation)
{
	_printLine(indentation, "WhileStatement");
	_printLine(indentation + 1, "_whileExpr");
	_whileExpr->get()->PrintTree(indentation + 2);
	_printLine(indentation + 1, "_stmtBlock");
	_stmtBlock->get()->PrintTree(indentation + 2);
}

void WhileStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	SymbolTable & whileScope = symbols.AddScope();
	_stmtBlock->FillSymbolTable(whileScope, true);
}

void WhileStatement::ResolveNames()
{
	_whileExpr->ResolveNames();
	_stmtBlock->ResolveNames();
}

void WhileStatement::TestTypes()
{
	_whileExpr->TestTypes();
	_stmtBlock->TestTypes();
	if (_whileExpr->getSymbolType() != Symbol::Type::Bool)
		SemanticErrors::AddError(SemanticErrors::Error(10, getLineNo(), {}));
}
