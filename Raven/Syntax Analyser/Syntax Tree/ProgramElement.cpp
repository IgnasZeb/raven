
#include "ProgramElement.h"

ProgramElement::ProgramElement()
{
}

ProgramElement::ProgramElement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "The size of nodes is incorrect!";

	_inner = nodes.front();
}

ProgramElement::~ProgramElement()
{
}

NodeTypes::NodeType ProgramElement::getNodeType()
{
	return NodeTypes::ProgramElement;
}

Node * ProgramElement::get()
{
	return _inner->get();
}

Node * ProgramElement::getInner()
{
	return _inner;
}

void ProgramElement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_inner->FillSymbolTable(symbols);
}

void ProgramElement::ResolveNames()
{
	_inner->ResolveNames();
}
