#include "IoStatement.h"
#include "IdentifierList.h"

IoStatement::IoStatement()
{
}

IoStatement::IoStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_ioOp = tkns.front();

	if (nodes.size() != 0)
	{
		if (_ioOp.type == TokenType::KWD_PRINT)
			_argList = nodes.front()->getInnerList();
		else
			_identList.swap(((IdentifierList*)nodes.front())->getIdentList());
	}
}


IoStatement::~IoStatement()
{
}

NodeTypes::NodeType IoStatement::getNodeType()
{
	return NodeTypes::IoStatement;
}

Node * IoStatement::get()
{
	return this;
}

std::list<Node*> IoStatement::getInnerList()
{
	return _argList;
}

std::string IoStatement::getName()
{
	return "IoStatement";
}

int IoStatement::getLineNo()
{
	return _ioOp.line;
}

void IoStatement::PrintTree(int indentation)
{
	_printLine(indentation, "IoStatement");
	_printLine(indentation + 1, "Type", _ioOp.GetName());
	_printLine(indentation + 1, "Args");

	if (_ioOp.type == TokenType::KWD_PRINT)
	{
		for (auto n : _argList)
			n->get()->PrintTree(indentation + 2);
	}
	else
	{
		for (auto & t : _identList)
			_printLine(indentation + 2, "Token", t.value.GetStringValue());
	}
}

void IoStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto a : _argList)
		a->FillSymbolTable(symbols);
}

void IoStatement::ResolveNames()
{
	for (auto a : _argList)
		a->ResolveNames();
	for (auto & t : _identList)
		scope->ResolveSymbol(t);
}

void IoStatement::TestTypes()
{
	for (auto a : _argList)
		a->TestTypes();
}
