#pragma once
#include "Node.h"

class Expression : public Node
{
private:
	Node * _inner;
public:
	Expression();
	Expression(std::list<Token> tkns, std::list<Node*> nodes);
	~Expression();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	std::string getName();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};