#pragma once
#include "Node.h"

class FunctionDefinition : public Node
{
private:
	Token _ident;
	Node * _type;
	Node * _parameters;
	Node * _statementBlock;
public:
	FunctionDefinition();
	FunctionDefinition(std::list<Token> tkns, std::list<Node*> nodes);
	~FunctionDefinition();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	std::list<Node*> getInnerList();
	Token getToken();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};