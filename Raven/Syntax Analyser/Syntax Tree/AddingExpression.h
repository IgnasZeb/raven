#pragma once
#include "Node.h"

class AddingExpression : public Node
{
private:
	Node * _inner = nullptr;
	Node * _left = nullptr;
	Node * _right = nullptr;
	Node * _innerOpNode = nullptr;
	Token _op;
public:
	AddingExpression();
	AddingExpression(std::list<Token> tkns, std::list<Node*> nodes);
	~AddingExpression();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::string getName();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};