#pragma once
#include "Node.h"

class Statement : public Node
{
private:
	Node * _inner;
public:
	Statement();
	Statement(std::list<Token> tkns, std::list<Node*> nodes);
	~Statement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};