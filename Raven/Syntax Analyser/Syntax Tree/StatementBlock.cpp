#include "StatementBlock.h"

StatementBlock::StatementBlock()
{
}

StatementBlock::StatementBlock(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() > 1)
		throw "The size of nodes list is incorrect!";

	for (auto n : nodes.front()->getInnerList())
	{
		_innerList.push_back(n);
	}

}

StatementBlock::~StatementBlock()
{
}

NodeTypes::NodeType StatementBlock::getNodeType()
{
	return NodeTypes::StatementBlock;
}

Node * StatementBlock::get()
{
	return this;
}

std::list<Node*> StatementBlock::getInnerList()
{
	return _innerList;
}

int StatementBlock::getLineNo()
{
	if (_innerList.size() != 0)
		return _innerList.front()->getLineNo();
	else
		return 0;
}

void StatementBlock::PrintTree(int indentation)
{
	_printLine(indentation, "StatementBlock");
	for (auto n : _innerList)
	{
		n->get()->PrintTree(indentation + 1);
	}
}

void StatementBlock::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	SymbolTable & blockScope = symbols;

	if (!suppressNewScope)
		blockScope = symbols.AddScope();

	for (auto n : _innerList)
	{
		n->FillSymbolTable(blockScope);
	}
}

void StatementBlock::ResolveNames()
{
	for (auto i : _innerList)
		i->ResolveNames();
}

void StatementBlock::TestTypes()
{
	for (auto i : _innerList)
		i->get()->TestTypes();
}
