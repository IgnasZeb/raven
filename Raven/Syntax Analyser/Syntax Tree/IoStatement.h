#pragma once
#include "Node.h"

class IoStatement : public Node
{
private:
	std::list<Node*> _argList;
	std::list<Token> _identList;
	Token _ioOp;
public:
	IoStatement();
	IoStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~IoStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};