#include "MultiplyingOperator.h"

MultiplyingOperator::MultiplyingOperator()
{
}

MultiplyingOperator::MultiplyingOperator(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_op = tkns.front();
}


MultiplyingOperator::~MultiplyingOperator()
{
}

NodeTypes::NodeType MultiplyingOperator::getNodeType()
{
	return NodeTypes::MultiplyingOperator;
}

Node * MultiplyingOperator::get()
{
	return this;
}

Token MultiplyingOperator::getToken()
{
	return _op;
}

std::string MultiplyingOperator::getName()
{
	return "MultiplyingOperator";
}