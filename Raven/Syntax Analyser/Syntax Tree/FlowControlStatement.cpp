#include "FlowControlStatement.h"

FlowControlStatement::FlowControlStatement()
{
}

FlowControlStatement::FlowControlStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "Invalid node list size!";

	_inner = nodes.front();
}


FlowControlStatement::~FlowControlStatement()
{
}

NodeTypes::NodeType FlowControlStatement::getNodeType()
{
	return NodeTypes::FlowControlStatement;
}

Node * FlowControlStatement::get()
{
	return _inner->get();
}

std::list<Node*> FlowControlStatement::getInnerList()
{
	throw "No inner array!";
}

Node * FlowControlStatement::getInner()
{
	return _inner->get();
}

int FlowControlStatement::getLineNo()
{
	return _inner->getLineNo();
}

void FlowControlStatement::ResolveNames()
{
	_inner->ResolveNames();
}

void FlowControlStatement::TestTypes()
{
	_inner->TestTypes();
}

void FlowControlStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);
	_inner->get()->FillSymbolTable(symbols);
}
