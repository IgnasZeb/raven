#pragma once
#include <list>
#include <iostream>
#include "../../Token.h"
#include "../../CodeWriter.h"
#include "../../Semantic Analyser/SymbolTable.h"
#include "NodeTypes.h"

class Scope;

class Node
{
protected:
	void _printLine(int indentation, std::string str1, std::string str2 = "");
public:
	SymbolTable * scope = nullptr;
	Node * parent = nullptr;
	//Scope * scope;

	Node();
	Node(std::list<Token> tkns, std::list<Node*> nodes);
	~Node();

	virtual NodeTypes::NodeType getNodeType();
	virtual Node * get();
	virtual std::list<Node*> getInnerList();
	virtual Node * getInner();
	virtual Token getToken();
	virtual std::string getName();
	virtual Symbol::Type getSymbolType();
	virtual int getLineNo();

	virtual void PrintTree(int indentation);

	// Semantic analysis
	virtual void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	virtual void ResolveNames();
	virtual void TestTypes();

	virtual void GenerateCode(CodeWriter & w);
};