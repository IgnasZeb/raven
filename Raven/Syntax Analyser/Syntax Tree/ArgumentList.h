#pragma once
#include "Node.h"

class ArgumentList : public Node
{
private:
	std::list<Node*> _arguments;
public:
	ArgumentList();
	ArgumentList(std::list<Token> tkns, std::list<Node*> nodes);
	~ArgumentList();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};