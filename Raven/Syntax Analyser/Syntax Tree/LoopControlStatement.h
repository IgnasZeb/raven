#pragma once
#include "Node.h"

class LoopControlStatement : public Node
{
private:
	Token _inner;
public:
	LoopControlStatement();
	LoopControlStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~LoopControlStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);
	
	void TestTypes();
};