#pragma once
#include "Node.h"

class Term : public Node
{
private:
	Node * _innerNode = nullptr;
	Token _innerToken = Token::InvalidToken;
public:
	Term();
	Term(std::list<Token> tkns, std::list<Node*> nodes);
	~Term();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	Token getToken();
	std::string getName();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};