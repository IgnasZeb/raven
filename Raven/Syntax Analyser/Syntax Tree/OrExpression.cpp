#include "OrExpression.h"
#include "../../Semantic Analyser/SemanticErrors.h"

OrExpression::OrExpression()
{
}

OrExpression::OrExpression(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 1)
		_inner = nodes.front();
	else
	{
		_left = nodes.front();
		_right = nodes.back();
	}
}


OrExpression::~OrExpression()
{
}

NodeTypes::NodeType OrExpression::getNodeType()
{
	return NodeTypes::OrExpression;
}

Node * OrExpression::get()
{
	if (_inner == nullptr)
		return this;
	else
		return _inner->get();
}

std::string OrExpression::getName()
{
	return "OrExpression";
}

void OrExpression::PrintTree(int indentation)
{
	if (_inner == nullptr)
	{
		_printLine(indentation, "OrExpression");
		_printLine(indentation + 1, "_left:");
		_left->get()->PrintTree(indentation + 2);
		_printLine(indentation + 1, "_right");
		_right->get()->PrintTree(indentation + 2);
	}
	else
	{
		_inner->get()->PrintTree(indentation + 1);
	}
}

void OrExpression::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	if (_inner != nullptr)
		_inner->FillSymbolTable(symbols);
	else
	{
		_left->FillSymbolTable(symbols);
		_right->FillSymbolTable(symbols);
	}
}

void OrExpression::ResolveNames()
{
	if (_inner == nullptr)
	{
		_left->ResolveNames();
		_right->ResolveNames();
	}
	else
		_inner->ResolveNames();
}

Symbol::Type OrExpression::getSymbolType()
{
	if (_inner != nullptr)
		return _inner->get()->getSymbolType();
	else
		return _left->get()->getSymbolType();
}

int OrExpression::getLineNo()
{
	if (_inner != nullptr)
		return _inner->getLineNo();
	else
		return _left->getLineNo();
}

void OrExpression::TestTypes()
{
	if (_inner == nullptr)
	{
		_left->TestTypes();
		_right->TestTypes();
		if (_left->get()->getSymbolType() != _right->get()->getSymbolType())
			SemanticErrors::AddError(SemanticErrors::Error(5, getLineNo(), { Symbol::GetTypeName(_left->getSymbolType()), Symbol::GetTypeName(_right->getSymbolType()) }));
	}
	else
		_inner->TestTypes();
}
