#include "Parameter.h"

Parameter::Parameter()
{
}

Parameter::Parameter(Node * t, Token i) : type(t), ident(i)
{
}


Parameter::~Parameter()
{
}

NodeTypes::NodeType Parameter::getNodeType()
{
	return NodeTypes::Parameter;
}

Node * Parameter::get()
{
	return this;
}

int Parameter::getLineNo()
{
	return ident.line;
}

void Parameter::PrintTree(int indentation)
{
	_printLine(indentation, "Parameter");
	_printLine(indentation + 1, "_type");
	type->get()->PrintTree(indentation+2);
	_printLine(indentation + 1, "ident");
	_printLine(indentation + 2, "Token", ident.value.GetStringValue());
}

void Parameter::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	symbols.AddSymbol(Symbol(Symbol::SymbolType::Variable, type->getSymbolType(), ident));
}

void Parameter::ResolveNames()
{
	scope->ResolveSymbol(ident);
}
