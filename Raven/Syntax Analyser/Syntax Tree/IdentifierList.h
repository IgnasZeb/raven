#pragma once
#include "Node.h"

class IdentifierList : public Node
{
private:
	std::list<Token> _identList;
public:
	IdentifierList();
	IdentifierList(std::list<Token> tkns, std::list<Node*> nodes);
	~IdentifierList();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::string getName();
	int getLineNo();

	std::list<Token> &getIdentList();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};