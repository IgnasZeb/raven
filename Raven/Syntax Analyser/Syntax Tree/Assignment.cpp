#include "Assignment.h"
#include "../../Semantic Analyser/SemanticErrors.h"

Assignment::Assignment()
{
}

Assignment::Assignment(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() != 2 || nodes.size() != 1)
		throw "Illegal argument sequence!";

	_left = tkns.front();
	_right = nodes.front();
}

Assignment::~Assignment()
{
}

NodeTypes::NodeType Assignment::getNodeType()
{
	return NodeTypes::Assignment;
}

Node * Assignment::get()
{
	return this;
}

Node * Assignment::getInner()
{
	throw "No inner!";
}

int Assignment::getLineNo()
{
	return _left.line;
}

void Assignment::PrintTree(int indentation)
{
	_printLine(indentation, "Assignment");
	_printLine(indentation + 1, "_left");
	_printLine(indentation + 1, "Token", _left.GetName());
	_printLine(indentation + 1, "_right");
	_right->get()->PrintTree(indentation + 2);
}

void Assignment::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_right->FillSymbolTable(symbols);
}

void Assignment::ResolveNames()
{
	scope->ResolveSymbol(_left);
	_right->ResolveNames();
}

void Assignment::TestTypes()
{
	_right->TestTypes();
	if(scope->ResolveSymbol(_left).type != _right->get()->getSymbolType())
		SemanticErrors::AddError(SemanticErrors::Error(5, getLineNo(), { Symbol::GetTokenTypeName(_left.type), Symbol::GetTypeName(_right->getSymbolType()) }));
}
