#include "Statement.h"

Statement::Statement()
{
}

Statement::Statement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "Invalid node list size!";

	_inner = nodes.front();
}

Statement::~Statement()
{
}

NodeTypes::NodeType Statement::getNodeType()
{
	return NodeTypes::Statement;
}

Node * Statement::get()
{
	return this;
}

Node * Statement::getInner()
{
	return _inner->get();
}

int Statement::getLineNo()
{
	return _inner->getLineNo();
}

void Statement::PrintTree(int indentation)
{
	_printLine(indentation, "Statement");
	_inner->get()->PrintTree(indentation + 1);
}

void Statement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_inner->get()->FillSymbolTable(symbols);
}

void Statement::ResolveNames()
{
	_inner->ResolveNames();
}

void Statement::TestTypes()
{
	_inner->TestTypes();
}
