#pragma once
#include "Node.h"

class OrExpression : public Node
{
private:
	Node * _inner = nullptr;
	Node * _left = nullptr;
	Node * _right = nullptr;
public:
	OrExpression();
	OrExpression(std::list<Token> tkns, std::list<Node*> nodes);
	~OrExpression();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::string getName();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};