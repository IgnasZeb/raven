#pragma once
#include "Node.h"

class Assignment : public Node
{
private:
	Token _left;
	Node * _right;
public:
	Assignment();
	Assignment(std::list<Token> tkns, std::list<Node*> nodes);
	~Assignment();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};