#include "IdentifierList.h"

IdentifierList::IdentifierList()
{
}

IdentifierList::IdentifierList(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 1)
	{
		std::list<Token> & oldList = ((IdentifierList*)nodes.front())->getIdentList();
		_identList.swap(oldList);
	}

	_identList.push_back(tkns.front());
}


IdentifierList::~IdentifierList()
{
}

NodeTypes::NodeType IdentifierList::getNodeType()
{
	return NodeTypes::IdentifierList;
}

Node * IdentifierList::get()
{
	return this;
}

std::string IdentifierList::getName()
{
	return "IdentifierList";
}

int IdentifierList::getLineNo()
{
	if (_identList.size() != 0)
		return _identList.front().line;
	else
		return 0;
}

std::list<Token>& IdentifierList::getIdentList()
{
	return _identList;
}

void IdentifierList::PrintTree(int indentation)
{
	_printLine(indentation, "IdentifierList");
	_printLine(indentation + 1, "_identList");
	for (auto & t : _identList)
		_printLine(indentation + 2, "Token", t.value.GetStringValue());
}

void IdentifierList::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);
}

void IdentifierList::ResolveNames()
{
	for (auto &t : _identList)
		scope->ResolveSymbol(t);
}
