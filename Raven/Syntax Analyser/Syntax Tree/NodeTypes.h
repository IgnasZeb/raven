#pragma once


namespace NodeTypes {
	enum NodeType {
		Undefined,
		Program,
		ProgramElements,
		ProgramElement,
		VariableInitialization,
		VariableDeclaration,
		FunctionDefinition,
		Type,
		StatementBlock,
		ParameterList,
		ArgumentList,
		Parameter,
		Statements,
		Statement,
		Assignment,
		FlowControlStatement,
		ReturnStatement,
		IfStatement,
		LoopControlStatement,
		ForStatement,
		WhileStatement,
		FunctionCall,
		IdentifierList,
		IoStatement,
		Expression,
		BooleanOrExpression,
		BooleanAndExpression,
		RelationalExpression,
		OrExpression,
		AddingExpression,
		MultiplyingExpression,
		Term,
		Constant,
		ArrayAccess,
		RelationalOperator,
		MultiplyingOperator,
		AddingOperator

	};
}