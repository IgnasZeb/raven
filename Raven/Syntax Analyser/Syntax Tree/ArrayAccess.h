#pragma once
#include "Node.h"

class ArrayAccess : public Node
{
private:
	Token _ident;
	Node * _index;
public:
	ArrayAccess();
	ArrayAccess(std::list<Token> tkns, std::list<Node*> nodes);
	~ArrayAccess();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	Token getToken();
	std::string getName();
};