#include "RelationalOperator.h"

RelationalOperator::RelationalOperator()
{
}

RelationalOperator::RelationalOperator(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_op = tkns.front();
}


RelationalOperator::~RelationalOperator()
{
}

NodeTypes::NodeType RelationalOperator::getNodeType()
{
	return NodeTypes::RelationalOperator;
}

Node * RelationalOperator::get()
{
	return this;
}

Token RelationalOperator::getToken()
{
	return _op;
}

std::string RelationalOperator::getName()
{
	return "RelationalOperator";
}