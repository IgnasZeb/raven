#pragma once
#include "Node.h"

class ProgramElement : public Node
{
private:
	Node * _inner;
public:
	ProgramElement();
	ProgramElement(std::list<Token> tkns, std::list<Node*> nodes);
	~ProgramElement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};