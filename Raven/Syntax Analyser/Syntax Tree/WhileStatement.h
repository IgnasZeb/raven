#pragma once
#include "Node.h"

class WhileStatement : public Node
{
private:
	Node * _whileExpr;
	Node * _stmtBlock;
public:
	WhileStatement();
	WhileStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~WhileStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};