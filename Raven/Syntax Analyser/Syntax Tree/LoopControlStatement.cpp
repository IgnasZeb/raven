#include "LoopControlStatement.h"
#include "../../Semantic Analyser/SemanticErrors.h"

LoopControlStatement::LoopControlStatement()
{
}

LoopControlStatement::LoopControlStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() != 1)
		throw "Invalid token list size!";

	_inner = tkns.front();
}

LoopControlStatement::~LoopControlStatement()
{
}

NodeTypes::NodeType LoopControlStatement::getNodeType()
{
	return NodeTypes::LoopControlStatement;
}

Node * LoopControlStatement::get()
{
	return this;
}

std::string LoopControlStatement::getName()
{
	return "LoopControlStatement";
}

int LoopControlStatement::getLineNo()
{
	return _inner.line;
}

void LoopControlStatement::PrintTree(int indentation)
{
	_printLine(indentation, "LoopControlStatement");
	_printLine(indentation + 1, "Token", _inner.GetName());
}

void LoopControlStatement::TestTypes()
{
	Node * loop = parent;
	while (loop != nullptr && loop->getNodeType() != NodeTypes::ForStatement && loop->getNodeType() != NodeTypes::WhileStatement)
		loop = loop->parent;

	if (loop == nullptr)
		SemanticErrors::AddError(SemanticErrors::Error(11, _inner.line, {}));
}
