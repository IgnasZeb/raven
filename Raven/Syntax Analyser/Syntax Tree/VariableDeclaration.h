#pragma once
#include "Node.h"

class VariableDeclaration : public Node
{
private:
	Token _ident;
	Node * _type;
public:
	VariableDeclaration();
	VariableDeclaration(std::list<Token> tkns, std::list<Node*> nodes);
	~VariableDeclaration();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	Token getToken();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
};