#include "ArgumentList.h"

ArgumentList::ArgumentList()
{
}

ArgumentList::ArgumentList(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 0 || nodes.size() > 2)
		throw "Invalid node list size!";

	if (nodes.size() == 2)
	{
		std::list<Node*> innerList = nodes.front()->getInnerList();
		_arguments.insert(_arguments.end(), innerList.begin(), innerList.end());
	}

	_arguments.push_back(nodes.back());
}


ArgumentList::~ArgumentList()
{
}

NodeTypes::NodeType ArgumentList::getNodeType()
{
	return NodeTypes::ArgumentList;
}

Node * ArgumentList::get()
{
	return this;
}

std::list<Node*> ArgumentList::getInnerList()
{
	return _arguments;
}

int ArgumentList::getLineNo()
{
	if (_arguments.size() != 0)
		return _arguments.front()->getLineNo();
	else
		return 0;
}

void ArgumentList::PrintTree(int indentation)
{
	_printLine(indentation, "ArgumentList");
	for (auto a : _arguments)
		a->get()->PrintTree(indentation+1);
}

void ArgumentList::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto n : _arguments)
		n->FillSymbolTable(symbols);
}

void ArgumentList::ResolveNames()
{
	for (auto a : _arguments)
		a->ResolveNames();
}

