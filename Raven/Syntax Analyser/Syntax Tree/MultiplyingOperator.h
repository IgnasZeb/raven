#pragma once
#include "Node.h"

class MultiplyingOperator : public Node
{
private:
	Token _op;
public:
	MultiplyingOperator();
	MultiplyingOperator(std::list<Token> tkns, std::list<Node*> nodes);
	~MultiplyingOperator();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Token getToken();
	std::string getName();
};