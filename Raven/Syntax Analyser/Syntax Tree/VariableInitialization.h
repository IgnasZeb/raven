#pragma once
#include "Node.h"

class VariableInitialization : public Node
{
private:
	Node * _variableDeclaration;
	Node * _expression;
public:
	VariableInitialization();
	VariableInitialization(std::list<Token> tkns, std::list<Node*> nodes);
	~VariableInitialization();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	Token getToken();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();

	void GenerateCode(CodeWriter & w);
};