#include "MultiplyingExpression.h"
#include "../../Semantic Analyser/SemanticErrors.h"

MultiplyingExpression::MultiplyingExpression()
{
}

MultiplyingExpression::MultiplyingExpression(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 1)
		_inner = nodes.front();
	else
	{
		_left = nodes.front();
		nodes.pop_front();
		_innerOpNode = nodes.front();
		_right = nodes.back();

		_op = _innerOpNode->getToken();
	}
}


MultiplyingExpression::~MultiplyingExpression()
{
}

NodeTypes::NodeType MultiplyingExpression::getNodeType()
{
	return NodeTypes::MultiplyingExpression;
}

Node * MultiplyingExpression::get()
{
	if (_inner == nullptr)
		return this;
	else
		return _inner->get();
}

std::string MultiplyingExpression::getName()
{
	return "MultiplyingExpression";
}

void MultiplyingExpression::PrintTree(int indentation)
{
	if (_inner == nullptr)
	{
		_printLine(indentation, "MultiplyingExpression");
		_printLine(indentation + 1, "_left:");
		_left->get()->PrintTree(indentation + 2);
		_printLine(indentation + 1, "_right");
		_right->get()->PrintTree(indentation + 2);
		_printLine(indentation + 1, "_op");
		_printLine(indentation + 1, "Token", _op.GetName());
	}
	else
	{
		_inner->get()->PrintTree(indentation + 1);
	}
}

void MultiplyingExpression::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	if (_inner != nullptr)
		_inner->FillSymbolTable(symbols);
	else
	{
		_left->FillSymbolTable(symbols);
		_right->FillSymbolTable(symbols);
	}
}

void MultiplyingExpression::ResolveNames()
{
	if (_inner == nullptr)
	{
		_left->ResolveNames();
		_right->ResolveNames();
	}
	else
		_inner->ResolveNames();
}

Symbol::Type MultiplyingExpression::getSymbolType()
{
	if (_inner != nullptr)
		return _inner->get()->getSymbolType();
	else
		return _left->get()->getSymbolType();
}

int MultiplyingExpression::getLineNo()
{
	if (_inner != nullptr)
		return _inner->getLineNo();
	else
		return _op.line;
}

void MultiplyingExpression::TestTypes()
{
	if (_inner == nullptr)
	{
		_left->TestTypes();
		_right->TestTypes();
		if (_left->get()->getSymbolType() != _right->get()->getSymbolType())
			SemanticErrors::AddError(SemanticErrors::Error(5, getLineNo(), { Symbol::GetTypeName(_left->getSymbolType()), Symbol::GetTypeName(_right->getSymbolType()) }));
	}
	else
		_inner->TestTypes();
}
