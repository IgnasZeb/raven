#include "Expression.h"

Expression::Expression()
{
}

Expression::Expression(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "Invalid node list size!";

	_inner = nodes.front();
}

Expression::~Expression()
{
}

NodeTypes::NodeType Expression::getNodeType()
{
	return NodeTypes::Expression;
}

Node * Expression::get()
{
	return _inner->get();
}

Node * Expression::getInner()
{
	return _inner->get();
}

std::string Expression::getName()
{
	return "Expression";
}

void Expression::PrintTree(int indentation)
{
	_printLine(indentation, "Expression");
	_inner->PrintTree(indentation + 1);
}

void Expression::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_inner->FillSymbolTable(symbols);
}

void Expression::ResolveNames()
{
	_inner->ResolveNames();
}

Symbol::Type Expression::getSymbolType()
{
	return _inner->get()->getSymbolType();
}

int Expression::getLineNo()
{
	return _inner->getLineNo();
}

void Expression::TestTypes()
{
	_inner->TestTypes();
}
