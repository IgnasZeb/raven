#include "Program.h"
#include "../../Semantic Analyser/SemanticErrors.h"


Program::Program()
{
}

Program::Program(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "The size of nodes list is incorrect!";

	_programElements = nodes.front();

	for (auto n : nodes.front()->get()->getInnerList())
	{
		inner.push_back(n->get());
		n->get()->parent = this;
	}
}


Program::~Program()
{
}

NodeTypes::NodeType Program::getNodeType()
{
	return NodeTypes::Program;
}

Node * Program::get()
{
	return this;
}

std::list<Node*> Program::getInnerList()
{
	return inner;
}

void Program::PrintTree(int indentation)
{
	_printLine(indentation, "Program");
	for (auto i : inner)
	{
		i->get()->PrintTree(indentation + 1);
	}
}

void Program::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto n : inner)
	{
		n->get()->FillSymbolTable(symbols);
	}
}

void Program::ResolveNames()
{
	for (auto i : inner)
		i->ResolveNames();
}

void Program::TestTypes()
{
	for (auto p : inner)
		p->get()->TestTypes();

	Node * func = (Node*)scope->ResolveFunction("main");
	if (func == nullptr)
		SemanticErrors::AddError(SemanticErrors::Error(12, 0, {}));

	if (func->getSymbolType() != Symbol::Type::Int)
		SemanticErrors::AddError(SemanticErrors::Error(13, 0, {}));
}

void Program::GenerateCode(CodeWriter & w)
{
	for (auto i : inner)
		i->GenerateCode(w);
}
