#pragma once
#include "Node.h"

class RelationalOperator : public Node
{
private:
	Token _op;
public:
	RelationalOperator();
	RelationalOperator(std::list<Token> tkns, std::list<Node*> nodes);
	~RelationalOperator();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Token getToken();
	std::string getName();
};