#include "ParameterList.h"
#include "Parameter.h"


ParameterList::ParameterList()
{
}

ParameterList::ParameterList(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 0 || nodes.size() > 2)
		throw "Invalid node list size!";

	if (nodes.size() == 2)
	{
		std::list<Node*> innerList = nodes.front()->getInnerList();
		_parameters.insert(_parameters.end(), innerList.begin(), innerList.end());
	}

	_parameters.push_back(new Parameter(nodes.back(), tkns.back()));
}


ParameterList::~ParameterList()
{
}

NodeTypes::NodeType ParameterList::getNodeType()
{
	return NodeTypes::ParameterList;
}

Node * ParameterList::get()
{
	return this;
}

std::list<Node*> ParameterList::getInnerList()
{
	return _parameters;
}

int ParameterList::getLineNo()
{
	if (_parameters.size() != 0)
		return _parameters.front()->getLineNo();
	else
		return 0;
}

void ParameterList::PrintTree(int indentation)
{
	_printLine(indentation, "ParameterList");
	for (auto n : _parameters)
		n->get()->PrintTree(indentation+1);
}

void ParameterList::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto p : _parameters)
	{
		p->FillSymbolTable(symbols);
	}
}

void ParameterList::ResolveNames()
{
	for (auto p : _parameters)
		p->ResolveNames();
}
