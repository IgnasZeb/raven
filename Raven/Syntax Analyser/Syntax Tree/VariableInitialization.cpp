#include "VariableInitialization.h"
#include "../../Semantic Analyser/SemanticErrors.h"

VariableInitialization::VariableInitialization()
{
}

VariableInitialization::VariableInitialization(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_variableDeclaration = nodes.front();
	if (nodes.size() == 2)
		_expression = nodes.back();
	else if (nodes.size() == 1)
		_expression = nullptr;
	else
		throw "The size of nodes is invalid!";
}


VariableInitialization::~VariableInitialization()
{
}

NodeTypes::NodeType VariableInitialization::getNodeType()
{
	if (_expression == nullptr)
		return NodeTypes::VariableDeclaration;
	else
		return NodeTypes::VariableInitialization;
}

Node * VariableInitialization::get()
{
	if (_expression == nullptr)
		return _variableDeclaration->get();
	else
		return this;
}

Node * VariableInitialization::getInner()
{
	return _variableDeclaration;
}

Token VariableInitialization::getToken()
{
	return _variableDeclaration->getToken();
}

int VariableInitialization::getLineNo()
{
	return _variableDeclaration->getLineNo();
}

void VariableInitialization::PrintTree(int indentation)
{
	_printLine(indentation, "VariableInitialization");
	_printLine(indentation + 1, "_variableDeclaration");
	_variableDeclaration->get()->PrintTree(indentation + 2);
	_printLine(indentation + 1, "_expression");
	_expression->get()->PrintTree(indentation + 2);
}

void VariableInitialization::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_variableDeclaration->FillSymbolTable(symbols);
}

void VariableInitialization::ResolveNames()
{
	if(_expression != nullptr)
		_expression->ResolveNames();
}

void VariableInitialization::TestTypes()
{
	_expression->TestTypes();
	Node * expr = _expression->get();
	if (Symbol::TokenTypeToSymbolType(_variableDeclaration->get()->getInner()->getToken().type) != _expression->get()->getSymbolType())
		SemanticErrors::AddError(SemanticErrors::Error(5, getLineNo(), { Symbol::GetTypeName(_variableDeclaration->getSymbolType()), Symbol::GetTypeName(_expression->getSymbolType()) }));
}

void VariableInitialization::GenerateCode(CodeWriter & w)
{
	_variableDeclaration->GenerateCode(w);
	if(_expression != 0)
	{
		
	}
}
