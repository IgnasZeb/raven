#pragma once
#include "Node.h"

class Constant : public Node
{
private:
	Token _inner;
public:
	Constant();
	Constant(std::list<Token> tkns, std::list<Node*> nodes);
	~Constant();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Token getToken();
	std::string getName();
	Symbol::Type getSymbolType();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
};