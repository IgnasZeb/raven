#include "Statements.h"

Statements::Statements()
{
}

Statements::Statements(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 0)
		throw "The size of nodes list is incorrect!";

	for (auto n : nodes)
	{
		if (n->getNodeType() == NodeTypes::Statement)
		{
			_stmt = n;

			_innerList.push_back(n->get());
		}
		else if (n->getNodeType() == NodeTypes::Statements)
		{
			_stmts = n;

			std::list<Node*> innerList = n->getInnerList();
			_innerList.insert(_innerList.end(), innerList.begin(), innerList.end());
		}
	}
}

Statements::~Statements()
{
}

NodeTypes::NodeType Statements::getNodeType()
{
	return NodeTypes::Statements;
}

Node * Statements::get()
{
	return this;
}

std::list<Node*> Statements::getInnerList()
{
	return _innerList;
}

void Statements::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto n : _innerList)
		n->FillSymbolTable(symbols);
}

void Statements::ResolveNames()
{
	if (_stmt != nullptr)
		_stmt->ResolveNames();
	if (_stmts != nullptr)
		_stmts->ResolveNames();
}
