#include "IfStatement.h"
#include "../../Semantic Analyser/SemanticErrors.h"

IfStatement::IfStatement()
{
}

IfStatement::IfStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() == 0)
		throw "Invalid IfStatement syntax!";
	
	if (tkns.front().type == TokenType::KWD_ELSE)
	{
		// <if_else_statement> :: = <if_statement> KWD_ELSE OP_COLON <statement_block>
		IfStatement * ifNode = (IfStatement*)nodes.front();
		ifBlocks.swap(ifNode->ifBlocks);
		elseBlock = nodes.back();
	}
	else if (nodes.size() == 2)
	{
		// <if_statement> ::= KWD_IF <expression> OP_COLON <statement_block>
		ifBlocks.push_back(std::make_pair(nodes.front(), nodes.back()));
	}
	else if (nodes.size() == 3)
	{
		// <if_statement> ::= <if_statement> KWD_ELIF <expression> OP_COLON <statement_block>
		IfStatement * ifNode = (IfStatement*)nodes.front();
		nodes.pop_front();
		ifBlocks.swap(ifNode->ifBlocks);
		ifBlocks.push_back(std::make_pair(nodes.front(), nodes.back()));
	}
	else
		throw "Invalid IfStatement syntax!";
}


IfStatement::~IfStatement()
{
}

NodeTypes::NodeType IfStatement::getNodeType()
{
	return NodeTypes::IfStatement;
}

Node * IfStatement::get()
{
	return this;
}

std::list<Node*> IfStatement::getInnerList()
{
	throw "No inner array!";
}

Node * IfStatement::getInner()
{
	throw "No inner!";
}

std::string IfStatement::getName()
{
	return "IfStatement";
}

int IfStatement::getLineNo()
{
	return ifBlocks.front().first->getLineNo();
}

void IfStatement::PrintTree(int indentation)
{
	for (auto & i : ifBlocks)
	{
		_printLine(indentation, "IfStatement");
		_printLine(indentation + 1, "Expr");
		i.first->get()->PrintTree(indentation + 2);
		_printLine(indentation + 1, "StmtBlock");
		i.second->get()->PrintTree(indentation + 2);
	}
	if (elseBlock != nullptr)
	{
		_printLine(indentation, "ElseStatement");
		_printLine(indentation + 1, "StmtBlock");
		elseBlock->get()->PrintTree(indentation + 2);
	}
}

void IfStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	for (auto & ifs : ifBlocks)
	{
		SymbolTable & ifScope = symbols.AddScope();
		ifs.first->get()->FillSymbolTable(symbols);
		ifs.second->get()->FillSymbolTable(ifScope, true);
	}
	if (elseBlock != nullptr)
	{
		SymbolTable & elseScope = symbols.AddScope();
		elseBlock->get()->FillSymbolTable(elseScope, true);
	}
}

void IfStatement::ResolveNames()
{
	for (auto & p : ifBlocks)
	{
		p.first->ResolveNames();
		p.second->ResolveNames();
	}
	if (elseBlock != nullptr)
		elseBlock->ResolveNames();
}

void IfStatement::TestTypes()
{
	for (auto p : ifBlocks)
	{
		p.first->TestTypes();
		p.second->TestTypes();
		if (p.first->get()->getSymbolType() != Symbol::Type::Bool)
			SemanticErrors::AddError(SemanticErrors::Error(3, 0, {})); // TOOD: FIX LINE NO
	}

	if (elseBlock != nullptr)
		elseBlock->TestTypes();
}
