#include "ArrayAccess.h"

ArrayAccess::ArrayAccess()
{
}

ArrayAccess::ArrayAccess(std::list<Token> tkns, std::list<Node*> nodes)
{
	_ident = tkns.front();
	_index = nodes.front();
}

ArrayAccess::~ArrayAccess()
{
}

NodeTypes::NodeType ArrayAccess::getNodeType()
{
	return NodeTypes::ArrayAccess;
}

Node * ArrayAccess::get()
{
	return this;
}

Node * ArrayAccess::getInner()
{
	return _index;
}

Token ArrayAccess::getToken()
{
	return _ident;
}

std::string ArrayAccess::getName()
{
	return "ArrayAccess";
}