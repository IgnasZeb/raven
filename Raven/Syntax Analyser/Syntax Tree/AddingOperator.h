#pragma once
#include "Node.h"

class AddingOperator : public Node
{
private:
	Token _op;
public:
	AddingOperator();
	AddingOperator(std::list<Token> tkns, std::list<Node*> nodes);
	~AddingOperator();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Token getToken();
	std::string getName();
};