#pragma once
#include "Node.h"

class Statements : public Node
{
private:
	Node * _stmt = nullptr;
	Node * _stmts = nullptr;

	std::list<Node *> _innerList;
public:
	Statements();
	Statements(std::list<Token> tkns, std::list<Node*> nodes);
	~Statements();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};