#include "ProgramElements.h"



ProgramElements::ProgramElements()
{
}

ProgramElements::ProgramElements(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 0)
		throw "The size of nodes list is incorrect!";
	
	for (auto n : nodes)
	{
		if (n->getNodeType() == NodeTypes::ProgramElement)
		{
			_programElem = n;
			inner.push_back(n->get());
		}
		else if (n->getNodeType() == NodeTypes::ProgramElements)
		{
			_programElems = n;
			std::list<Node*> innerList = n->getInnerList();
			inner.insert(inner.end(), innerList.begin(), innerList.end());
		}
	}
}


ProgramElements::~ProgramElements()
{
}

NodeTypes::NodeType ProgramElements::getNodeType()
{
	return NodeTypes::ProgramElements;
}

Node * ProgramElements::get()
{
	return this;
}

std::list<Node*> ProgramElements::getInnerList()
{
	return inner;
}

void ProgramElements::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	for (auto n : inner)
		n->FillSymbolTable(symbols);
}

void ProgramElements::ResolveNames()
{
	for (auto i : inner)
		i->ResolveNames();
}
