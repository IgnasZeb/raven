#include "Constant.h"

Constant::Constant()
{
}

Constant::Constant(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_inner = tkns.front();
}


Constant::~Constant()
{
}

NodeTypes::NodeType Constant::getNodeType()
{
	return NodeTypes::Constant;
}

Node * Constant::get()
{
	return this;
}

Token Constant::getToken()
{
	return _inner;
}

std::string Constant::getName()
{
	return "Constant";
}

void Constant::PrintTree(int indentation)
{
	_printLine(indentation, "Constant");
	_printLine(indentation + 1, "Token", _inner.GetName());
}

void Constant::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);
}

Symbol::Type Constant::getSymbolType()
{
	switch (_inner.type)
	{
	case TokenType::LIT_INT:
		return Symbol::Type::Int;
	case LIT_DEC:
		return Symbol::Type::Decimal;
	case LIT_STR:
		return Symbol::Type::String;
	default:
		return Symbol::Type::String;
	}
}

int Constant::getLineNo()
{
	return _inner.line;
}
