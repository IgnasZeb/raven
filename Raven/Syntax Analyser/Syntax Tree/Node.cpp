#include "Node.h"
#include <sstream>

void Node::_printLine(int indentation, std::string str1, std::string str2)
{
	std::stringstream ss;
	while (indentation > 0)
	{
		ss << "  ";
		indentation--;
	}

	ss << str1 << ": " << str2 << "\n";
	std::cout << ss.str();
}

Node::Node()
{
}

Node::Node(std::list<Token> tkns, std::list<Node*> nodes)
{
	for (auto n : nodes)
	{
		n->parent = this;
	//	n->scope->parent = scope;
	//	scope->ProcessNode(n);
	}
}


Node::~Node()
{
}

NodeTypes::NodeType Node::getNodeType()
{
	return NodeTypes::Undefined;
}

Node * Node::get()
{
	throw "Node does not implement get!";
}

std::list<Node*> Node::getInnerList()
{
	throw "No inner array!";
}

Node * Node::getInner()
{
	throw "No inner!";
}

Token Node::getToken()
{
	throw "No token!";
}

std::string Node::getName()
{
	return "Node";
}

Symbol::Type Node::getSymbolType()
{
	return Symbol::Type::Void;
}

int Node::getLineNo()
{
	throw "Not implemented";
}

void Node::PrintTree(int indentation)
{
	throw "Printing not implemented in class Node";
}

void Node::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	scope = &symbols;
}

void Node::ResolveNames()
{
}

void Node::TestTypes()
{
}

void Node::GenerateCode(CodeWriter & w)
{
	throw "Code generation not implemented";
}
