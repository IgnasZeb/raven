#include "Term.h"

Term::Term()
{
}

Term::Term(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() != 0 && (tkns.front().type == TokenType::IDENT || tkns.front().type == TokenType::OP_BIN_NOT))
		_innerToken = tkns.front();

	if (nodes.size() != 0)
		_innerNode = nodes.front();
}

Term::~Term()
{
}

NodeTypes::NodeType Term::getNodeType()
{
	return NodeTypes::Term;
}

Node * Term::get()
{
	return this;
}

Node * Term::getInner()
{
	return _innerNode;
}

Token Term::getToken()
{
	return _innerToken;
}

std::string Term::getName()
{
	return "Term";
}

void Term::PrintTree(int indenetation)
{
	_printLine(indenetation, "Term");
	if (_innerNode == nullptr)
	{
		_printLine(indenetation + 1, "Token", _innerToken.value.GetStringValue());
	}
	else
	{
		_innerNode->get()->PrintTree(indenetation + 1);
	}
}

void Term::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	if (_innerNode != nullptr)
		_innerNode->FillSymbolTable(symbols);
}

void Term::ResolveNames()
{
	if (_innerNode != nullptr)
		_innerNode->ResolveNames();
	if (!(_innerToken == Token::InvalidToken))
		scope->ResolveSymbol(_innerToken);
}

Symbol::Type Term::getSymbolType()
{
	if (_innerNode != nullptr)
		return _innerNode->get()->getSymbolType();
	else
		return scope->ResolveSymbol(_innerToken).type;
}

int Term::getLineNo()
{
	if (_innerNode == nullptr)
		return _innerToken.line;
	else
		return _innerNode->getLineNo();
}

void Term::TestTypes()
{
	if (_innerNode != nullptr)
		_innerNode->TestTypes();
}
