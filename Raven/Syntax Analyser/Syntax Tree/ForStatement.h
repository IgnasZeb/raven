#pragma once
#include "Node.h"

class ForStatement : public Node
{
private:
	Node * _from;
	Node * _to;
	Node * _step = nullptr;
	Node * _stmtBlock;
	Token _as;
public:
	ForStatement();
	ForStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~ForStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool newScope = false);
	void ResolveNames();
	void TestTypes();
};