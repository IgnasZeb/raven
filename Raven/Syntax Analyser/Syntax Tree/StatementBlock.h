#pragma once
#include "Node.h"

class StatementBlock : public Node
{
private:
	std::list<Node*> _innerList;
public:
	StatementBlock();
	StatementBlock(std::list<Token> tkns, std::list<Node*> nodes);
	~StatementBlock();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};