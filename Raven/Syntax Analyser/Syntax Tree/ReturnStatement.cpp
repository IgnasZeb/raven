#include "ReturnStatement.h"

#include "../../Semantic Analyser/SemanticErrors.h"
ReturnStatement::ReturnStatement()
{
	_right = nullptr;
}

ReturnStatement::ReturnStatement(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() == 1)
	{
		_right = nodes.front();
	}
	else
		_right = nullptr;
}

ReturnStatement::~ReturnStatement()
{
}

NodeTypes::NodeType ReturnStatement::getNodeType()
{
	return NodeTypes::ReturnStatement;
}

Node * ReturnStatement::get()
{
	return this;
}

std::list<Node*> ReturnStatement::getInnerList()
{
	throw "No inner array!";
}

Node * ReturnStatement::getInner()
{
	return _right;
}

int ReturnStatement::getLineNo()
{
	return _right->getLineNo();
}

void ReturnStatement::PrintTree(int indentation)
{
	_printLine(indentation, "ReturnStatement");
	_right->get()->PrintTree(indentation + 1);
}

void ReturnStatement::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_right->FillSymbolTable(symbols);
}

void ReturnStatement::ResolveNames()
{
	_right->ResolveNames();
}

void ReturnStatement::TestTypes()
{
	_right->TestTypes();
	Node * func = parent;
	while (func->getNodeType() != NodeTypes::FunctionDefinition)
		func = func->parent;

	if (func != nullptr)
	{
		if (func->get()->getSymbolType() != _right->get()->getSymbolType())
			SemanticErrors::AddError(SemanticErrors::Error(9, getLineNo(), { Symbol::GetTypeName(func->get()->getSymbolType()), Symbol::GetTypeName(_right->get()->getSymbolType()) }));
	}
	else
		SemanticErrors::AddError(SemanticErrors::Error(14, getLineNo(), {}));
}
