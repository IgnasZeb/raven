#pragma once
#include "Node.h"

class IfStatement : public Node
{
public:
	std::list<std::pair<Node*, Node *>> ifBlocks;
	Node * elseBlock = nullptr;

	IfStatement();
	IfStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~IfStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	Node * getInner();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};