#pragma once
#include "Node.h"

class ParameterList : public Node
{
private:
	std::list<Node*> _parameters;
public:
	ParameterList();
	ParameterList(std::list<Token> tkns, std::list<Node*> nodes);
	~ParameterList();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};