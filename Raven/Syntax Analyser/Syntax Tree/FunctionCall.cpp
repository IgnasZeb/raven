#include "FunctionCall.h"
#include "../../Semantic Analyser/SemanticErrors.h"

FunctionCall::FunctionCall()
{
}

FunctionCall::FunctionCall(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_ident = tkns.front();

	if (nodes.size() == 1)
		_argumentList = nodes.front();
}


FunctionCall::~FunctionCall()
{
}

NodeTypes::NodeType FunctionCall::getNodeType()
{
	return NodeTypes::FunctionCall;
}

Node * FunctionCall::get()
{
	return this;
}

Node * FunctionCall::getInner()
{
	return _argumentList;
}

std::string FunctionCall::getName()
{
	return "FunctionCall";
}

int FunctionCall::getLineNo()
{
	return _ident.line;
}

void FunctionCall::PrintTree(int indentation)
{
	_printLine(indentation, "FunctionCall");
	_printLine(indentation + 1, "_ident");
	_printLine(indentation + 2, "Token", _ident.value.GetStringValue());

	if (_argumentList != nullptr)
	{
		_printLine(indentation + 1, "_argumentList");
		_argumentList->get()->PrintTree(indentation+1);
	}
}

void FunctionCall::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	_argumentList->FillSymbolTable(symbols);
}

void FunctionCall::ResolveNames()
{
	scope->ResolveSymbol(_ident);
	if (_argumentList != nullptr)
		_argumentList->ResolveNames();
}

void FunctionCall::TestTypes()
{
	if (scope->ResolveSymbol(_ident).symbolType != Symbol::SymbolType::Function)
		SemanticErrors::AddError(SemanticErrors::Error(6, _ident.line, { _ident.value.GetStringValue() }));

	Node *func = (Node*)scope->ResolveFunction(_ident.value.GetStringValue());
	if (func == nullptr)
		SemanticErrors::AddError(SemanticErrors::Error(2, _ident.line, { _ident.value.GetStringValue() }));

	std::list<Node*> params = func->getInnerList();

	if (_argumentList != nullptr)
	{
		if (params.size() != _argumentList->getInnerList().size())
			SemanticErrors::AddError(SemanticErrors::Error(7, _ident.line, { _ident.value.GetStringValue(), SemanticErrors::IntToString(params.size()), SemanticErrors::IntToString(_argumentList->getInnerList().size()) }));

		for (std::list<Node*>::iterator pi = params.begin(), ai = _argumentList->getInnerList().begin(); pi != params.end(); ++pi, ++ai)
		{
			(*ai)->TestTypes();
			if ((*pi)->getSymbolType() != (*ai)->getSymbolType())
				SemanticErrors::AddError(SemanticErrors::Error(8, _ident.line, { Symbol::GetTypeName((*pi)->getSymbolType()), Symbol::GetTypeName((*ai)->getSymbolType()) }));
		}
	}
	else if(params.size() != 0)
		SemanticErrors::AddError(SemanticErrors::Error(7, _ident.line, { _ident.value.GetStringValue(), SemanticErrors::IntToString(params.size()), SemanticErrors::IntToString(_argumentList->getInnerList().size()) }));
}

Symbol::Type FunctionCall::getSymbolType()
{
	Node * func = (Node*)scope->ResolveFunction(_ident.value.GetStringValue());
	if (func == nullptr)
		SemanticErrors::AddError(SemanticErrors::Error(2, _ident.line, { _ident.value.GetStringValue() }));

	return func->getSymbolType();
}
