#pragma once
#include "Node.h"

class Program : public Node
{
private:
	Node * _programElements;
	std::list<Node*> inner;
public:
	Program();
	Program(std::list<Token> tkns, std::list<Node*> nodes);
	~Program();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();

	void GenerateCode(CodeWriter & w);
};

