#include "AddingOperator.h"

AddingOperator::AddingOperator()
{
}

AddingOperator::AddingOperator(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	_op = tkns.front();
}


AddingOperator::~AddingOperator()
{
}

NodeTypes::NodeType AddingOperator::getNodeType()
{
	return NodeTypes::AddingOperator;
}

Node * AddingOperator::get()
{
	return this;
}

Token AddingOperator::getToken()
{
	return _op;
}

std::string AddingOperator::getName()
{
	return "AddingOperator";
}