#pragma once
#include "Node.h"

class FlowControlStatement : public Node
{
private:
	Node * _inner;
public:
	FlowControlStatement();
	FlowControlStatement(std::list<Token> tkns, std::list<Node*> nodes);
	~FlowControlStatement();

	NodeTypes::NodeType getNodeType();
	Node * get();
	std::list<Node*> getInnerList();
	Node * getInner();
	int getLineNo();

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
	void TestTypes();
};