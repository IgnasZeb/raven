#pragma once
#include "Node.h"

class FunctionCall : public Node
{
private:
	Token _ident;
	Node * _argumentList = nullptr;
public:
	FunctionCall();
	FunctionCall(std::list<Token> tkns, std::list<Node*> nodes);
	~FunctionCall();

	NodeTypes::NodeType getNodeType();
	Node * get();
	Node * getInner();
	std::string getName();
	int getLineNo();

	void PrintTree(int indentation);

	void ResolveNames();

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void TestTypes();
	Symbol::Type getSymbolType();
};