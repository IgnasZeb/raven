#include "VariableDeclaration.h"

VariableDeclaration::VariableDeclaration()
{
}

VariableDeclaration::VariableDeclaration(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (nodes.size() != 1)
		throw "Invalid node list size!";

	if (tkns.size() != 1)
		throw "Invalid token list size!";

	_ident = tkns.front();
	_type = nodes.front();
}

VariableDeclaration::~VariableDeclaration()
{
}

NodeTypes::NodeType VariableDeclaration::getNodeType()
{
	return NodeTypes::VariableDeclaration;
}

Node * VariableDeclaration::get()
{
	return this;
}

Node * VariableDeclaration::getInner()
{
	return _type;
}

Token VariableDeclaration::getToken()
{
	return _ident;
}

int VariableDeclaration::getLineNo()
{
	return _ident.line;
}

void VariableDeclaration::PrintTree(int indentation)
{
	_printLine(indentation, "VariableDeclaration");
	_printLine(indentation + 1, "_ident");
	_printLine(indentation + 2, "Token", _ident.value.GetStringValue());
	_printLine(indentation + 1, "_type");
	_type->get()->PrintTree(indentation+2);
}

void VariableDeclaration::FillSymbolTable(SymbolTable & symbols, bool suppressNewScope)
{
	Node::FillSymbolTable(symbols, suppressNewScope);

	symbols.AddSymbol(Symbol(Symbol::SymbolType::Variable, _type->getSymbolType(), _ident));
}
