#pragma once
#include "Node.h"

class Parameter : public Node
{
public:
	Node * type;
	Token ident;

	Parameter();
	Parameter(Node * t, Token i);
	~Parameter();

	NodeTypes::NodeType getNodeType();
	Node * get();
	int getLineNo();

	void PrintTree(int indentation);

	void FillSymbolTable(SymbolTable &symbols, bool suppressNewScope = false);
	void ResolveNames();
};