#include "Type.h"

Type::Type()
{
}

Type::Type(std::list<Token> tkns, std::list<Node*> nodes) : Node(tkns, nodes)
{
	if (tkns.size() != 1)
		throw "Invalid token list size!";
	if (nodes.size() != 0)
		throw "Invalid node list size!";

	_type = tkns.front();
}

Type::~Type()
{
}

NodeTypes::NodeType Type::getNodeType()
{
	return NodeTypes::Type;
}

Node * Type::get()
{
	return this;
}

Node * Type::getInner()
{
	throw "No inner!";
}

Token Type::getToken()
{
	return _type;
}

void Type::PrintTree(int indentation)
{
	_printLine(indentation, "Type");
	_printLine(indentation + 1, "Token", _type.GetName());
}

Symbol::Type Type::getSymbolType()
{
	//KWD_INT | KWD_BOOL | KWD_DECIMAL | KWD_STRING | KWD_VOID
	switch (_type.type)
	{
	case TokenType::KWD_INT:
		return Symbol::Type::Int;
	case TokenType::KWD_BOOL:
		return Symbol::Type::Bool;
	case TokenType::KWD_DECIMAL:
		return Symbol::Type::Decimal;
	case TokenType::KWD_STRING:
		return Symbol::Type::String;
	case TokenType::KWD_VOID:
		return Symbol::Type::Void;
	}
}

int Type::getLineNo()
{
	return _type.line;
}
