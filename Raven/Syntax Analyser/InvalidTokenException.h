#pragma once
#include "../Token.h"
#include <stdexcept>

class InvalidTokenException :
	public std::invalid_argument
{
public:
	uint16_t errorCode;
	Token token;

	InvalidTokenException();
	InvalidTokenException(const std::string &_Message);
	InvalidTokenException(const std::string &_Message,  uint16_t errCode);
	InvalidTokenException(Token &tkn, uint16_t errCode);
	~InvalidTokenException();
};

