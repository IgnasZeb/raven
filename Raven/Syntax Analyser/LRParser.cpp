#include "LRParser.h"
#include "LRTable.h"
#include "Syntax Tree/Program.h"

#undef _DEBUG

#include <iostream>
#include <iomanip>
#include "InvalidTokenException.h"

#ifdef _DEBUG
std::string getStackStr(std::stack <std::string> stack)
{
	std::list<std::string> symbolList;
	while (!stack.empty())
	{
		symbolList.push_front(stack.top());
		stack.pop();
	}
	std::string result = "";
	for (std::string &symbol : symbolList)
	{
		result += symbol + " ";;
	}
	return result;
}
std::string getStackStr(std::stack <int> stack)
{
	std::list<std::string> symbolList;
	while (!stack.empty())
	{
		char buffer[10];
		_itoa_s(stack.top(), buffer, 10);
		symbolList.push_front(std::string(buffer));
		stack.pop();
	}
	std::string result = "";
	for (std::string &symbol : symbolList)
	{
		result += symbol + " ";;
	}
	return result;
}

std::ostream &stream = std::cout;
#endif

Token LRParser::GetNextToken()
{
	if (_inputTokensIndex == _inputTokens.size())
		return _inputTokens[_inputTokensIndex - 1];

	return _inputTokens[_inputTokensIndex++];
}
void LRParser::InvokeError()
{
	throw InvalidTokenException(_lookaheadSymbol, _state);
}

LRParser::LRParser(std::vector<Token> &tokens) : _inputTokens(tokens)
{
}


LRParser::~LRParser()
{
}

void LRParser::Parse()
{
	int iter = 0;
	_stateStack.push(0);
	std::stack<std::string> totalStack;

	while (!_stateStack.empty())
	{
		if (_lookaheadSymbol == Token::InvalidToken)
			_lookaheadSymbol = GetNextToken();

		Action &action = LR::ActionTable[_state][_lookaheadSymbol.type];

#ifdef _DEBUG
		stream << std::left << std::setw(40) << getStackStr(_stateStack) << std::setw(150) << getStackStr(totalStack) << std::setw(20) << _lookaheadSymbol.GetName() << ((action.Type == Action::Shift) ? "s" : "r") << action.State << "\n";
#endif // _DEBUG

		if (action.Type == Action::Shift)
		{
			_termStack.push(_lookaheadSymbol);
#ifdef _DEBUG
			totalStack.push(_lookaheadSymbol.GetName());

#endif // _DEBUG
			_lookaheadSymbol = Token::InvalidToken;

			_state = action.State;
			_stateStack.push(_state);
		}
		else if (action.Type == Action::Reduce)
		{
			if (action.State == 0)
			{
				if (_lookaheadSymbol.type == TokenType::_EOF)
					break;
				else
					InvokeError();
			}
			else
			{
				std::list<Token> tkns;
				std::list<Node*> nodes;

				for (int i = 0; i < LR::RuleTable[action.State].TerminalCount; i++)
				{
					tkns.push_front(_termStack.top());
					_termStack.pop();
#ifdef _DEBUG
					totalStack.pop();

#endif // _DEBUG
				}
				for (int i = 0; i < LR::RuleTable[action.State].NonTerminalCount; i++)
				{
					nodes.push_front(_nontermStack.top());
					_nontermStack.pop();
#ifdef _DEBUG
					totalStack.pop();
#endif // _DEBUG

				}
				_nontermStack.push(LR::ReduceActionTable[action.State](tkns, nodes));
				//_nontermStack.push(LR::RuleTable[action.State].LHSName);
#ifdef _DEBUG
				totalStack.push(LR::RuleTable[action.State].LHSName);
#endif // _DEBUG

				for (int i = 0; i < LR::RuleTable[action.State].NonTerminalCount + LR::RuleTable[action.State].TerminalCount; i++)
					_stateStack.pop();

				_state = LR::GotoTable[_stateStack.top()][LR::RuleTable[action.State].LHSName];
#ifdef _DEBUG
				stream << std::left << std::setw(40) << getStackStr(_stateStack) << std::setw(150) << getStackStr(totalStack) << std::setw(20) << _lookaheadSymbol.GetName() << _state << "\n";

#endif // _DEBUG
				//_stateStack.pop();
				_stateStack.push(_state);
			}

		}
		else
			InvokeError();
	}

	if (_nontermStack.size() != 1)
		throw "Parsing done, but there are still items to reduce";

	rootNode = new Program(std::list<Token>(), { _nontermStack.top() });
}
