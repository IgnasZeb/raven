#pragma once

#include <stack>
#include "../Token.h"
#include "Syntax Tree/Node.h"

class LRParser
{
private:
	std::vector<Token> &_inputTokens;
	int _inputTokensIndex = 0;

	std::stack<int> _stateStack;
	std::stack<Token> _termStack;
	std::stack<Node*> _nontermStack;
	
	Token _lookaheadSymbol = Token::InvalidToken;
	int _state = 0;

	Token GetNextToken();
	void InvokeError();
public:
	Node *rootNode;

	LRParser(std::vector<Token> &tokens);
	~LRParser();

	void Parse();
};

