#pragma once

#include <map>
#include <vector>
#include <string>
#include <functional>
#include "../Token.h"
#include "Syntax Tree/Node.h"

struct Action
{
	enum ActionType { Error, Shift, Reduce, Goto };
	ActionType Type;
	int State;
	Action() : State(-1), Type(Error) {};
	Action(ActionType a, int s) : Type(a), State(s) {}
};

namespace LR
{
	struct Rule
	{
		int TerminalCount;
		int NonTerminalCount;
		std::string LHSName;
		Rule() {};
		Rule(int tc, int nc, std::string lhs) : TerminalCount(tc), NonTerminalCount(nc), LHSName(lhs) {}
	};

	extern std::vector<Rule> RuleTable;
	extern std::map<int, std::map<TokenType, Action>> ActionTable;
	extern std::map<int, std::map<std::string, int>> GotoTable;
	extern std::vector<std::function<Node*(std::list<Token>, std::list<Node*>)>> ReduceActionTable;
}