#include "InvalidTokenException.h"

InvalidTokenException::InvalidTokenException() : invalid_argument("")
{
}

InvalidTokenException::InvalidTokenException(const std::string & _Message) : invalid_argument(_Message)
{
	errorCode = 0;
}

InvalidTokenException::InvalidTokenException(const std::string & _Message, uint16_t errCode) : invalid_argument(_Message), errorCode(errCode), token()
{
}

InvalidTokenException::InvalidTokenException(Token & tkn, uint16_t errCode) : invalid_argument(""), errorCode(errCode), token(tkn)
{
}

InvalidTokenException::~InvalidTokenException()
{
}
