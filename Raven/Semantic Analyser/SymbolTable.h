#pragma once
#include <vector>
#include <list>
#include <map>
#include "Symbol.h"

class SymbolTable
{
public:
	std::list<Symbol> symbols;
	std::map<std::string, void*> functionNodes;
	std::list<SymbolTable> subScopes;
	std::list<Token> constants;
	SymbolTable * parent = nullptr;

	SymbolTable();
	SymbolTable(SymbolTable * p);

	void AddSymbol(Symbol symbol);
	void AddSymbol(Symbol symbol, void * node);
	void AddConstant(Token c);
	SymbolTable & AddScope();

	Symbol & ResolveSymbol(Token name);
	void * ResolveFunction(std::string name);

	~SymbolTable();
};

