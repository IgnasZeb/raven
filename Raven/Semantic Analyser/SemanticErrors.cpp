#include "SemanticErrors.h"

#include <list>
#include <iomanip>

namespace SemanticErrors
{
	namespace
	{
		std::list<Error> semanticErrors;

		std::string FormatError(const Error & error, std::string fileName)
		{
			std::string result;
			char * buffer = nullptr;
			int len = 0;

			switch (error.errorCode)
			{
			case 1:
				len = _scprintf("Unidentified identifier '%s'", error.args[0].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Unidentified identifier '%s'", error.args[0].c_str());
				break;
			case 2:
				len = _scprintf("Undeclared function '%s'", error.args[0].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Undeclared function '%s'", error.args[0].c_str());
				break;
			case 3:
				len = _scprintf("If expression must be boolean") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "If expression must be boolean");
				break;
			case 4:
				len = _scprintf("For loop range expressions must be integer") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "For loop range expressions must be integer");
				break;
			case 5:
				len = _scprintf("Operand type mismatch (%s and %s)", error.args[0].c_str(), error.args[1].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Operand type mismatch (%s and %s)", error.args[0].c_str(), error.args[1].c_str());
				break;
			case 6:
				len = _scprintf("Variable '%s' is not a function", error.args[0].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Variable '%s' is not a function", error.args[0].c_str());
				break;
			case 7:
				len = _scprintf("Function '%s' accepts %s parameters, %s were given", error.args[0].c_str(), error.args[1].c_str(), error.args[2].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Function '%s' accepts %s parameters, %s were given", error.args[0].c_str(), error.args[1].c_str(), error.args[2].c_str());
				break;
			case 8:
				len = _scprintf("Invalid argument type: expected %s, found %s", error.args[0].c_str(), error.args[1].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Invalid argument type: expected %s, found %s", error.args[0].c_str(), error.args[1].c_str());
				break;
			case 9:
				len = _scprintf("Function returns %s, attempted to return %s", error.args[0].c_str(), error.args[1].c_str()) + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Function returns %s, attempted to return %s", error.args[0].c_str(), error.args[1].c_str());
				break;
			case 10:
				len = _scprintf("While statement expression must be boolean") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "While statement expression must be boolean");
				break;
			case 11:
				len = _scprintf("Loop control statement outside of loop") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Loop control statement outside of loop");
				break;
			case 12:
				len = _scprintf("Program entry point function 'main' not found") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Program entry point function 'main' not found");
				break;
			case 13:
				len = _scprintf("Function 'main' must return integer") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Function 'main' must return integer");
				break;
			case 14:
				len = _scprintf("Return outside of function") + 2;
				buffer = new char[len];
				_snprintf_s(buffer, len, len, "Return outside of function");
				break;
			}

			char * finalMessage;
			len = _scprintf("%s(%lu) : error S%03u: %s", fileName.c_str(), error.lineNo, error.errorCode, buffer) + 2;
			finalMessage = new char[len];
			_snprintf_s(finalMessage, len, len, "%s(%lu) : error S%03u: %s", fileName.c_str(), error.lineNo, error.errorCode, buffer);
			
			result = finalMessage;

			delete[] buffer;
			delete[] finalMessage;

			return result;
		}
	}

	Error::Error(unsigned int code, unsigned int line, std::vector<std::string> arg) : errorCode(code), lineNo(line), args(arg)
	{
	}

	void InitSemanticErrors()
	{
		semanticErrors.clear();
	}
	void AddError(Error error)
	{
		semanticErrors.push_back(error);
	}

	void PrintErrors(std::ostream & output, std::string fileName)
	{
		for (const auto & e : semanticErrors)
		{
			output << FormatError(e, fileName).c_str() << "\n";
		}
	}
	bool HasErrors()
	{
		return semanticErrors.size() != 0;
	}
	std::string IntToString(int i)
	{
		char buff[10];
		_itoa_s(i, buff, 10, 10);
		return buff;
	}
}
