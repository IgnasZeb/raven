#include "SemanticAnalyser.h"
#include "SemanticErrors.h"

SemanticAnalyser::SemanticAnalyser(Node * root) : rootNode(root)
{
}

SemanticAnalyser::~SemanticAnalyser()
{
}

bool SemanticAnalyser::Verify()
{
	rootNode->FillSymbolTable(globalSymbolTable);
	rootNode->ResolveNames();
	rootNode->TestTypes();

	return !SemanticErrors::HasErrors();
}
