#include "SymbolTableException.h"

SymbolTableException::SymbolTableException() : invalid_argument("")
{
}

SymbolTableException::SymbolTableException(const std::string & _Message) : invalid_argument(_Message)
{
	errorCode = 0;
}

SymbolTableException::SymbolTableException(const std::string & _Message, uint16_t errCode, int lineNo) : invalid_argument(_Message), errorCode(errCode), token()
{
	token.line = lineNo;
}

SymbolTableException::SymbolTableException(char * str, uint16_t errCode, int lineNo) : invalid_argument(str), errorCode(errCode), token()
{
	token.line = lineNo;
}

SymbolTableException::SymbolTableException(Token & tkn, uint16_t errCode) : invalid_argument(""), errorCode(errCode), token(tkn)
{
}

SymbolTableException SymbolTableException::InvokeIncorrectTypesException(Symbol::Type type1, Symbol::Type type2, int lineNo)
{
	char errorBuffer[500];
	Token t1, t2;
	sprintf_s(errorBuffer, 500, "Operand types must be matching, %s and %s given", Symbol::GetTypeName(type1).c_str(), Symbol::GetTypeName(type2).c_str());
	t1.line = lineNo;
	SymbolTableException ex(errorBuffer, 3, lineNo);
	ex.token = t1;
	throw ex;
}

SymbolTableException::~SymbolTableException()
{
}
