#pragma once
#include "../Token.h"
#include "Symbol.h"
#include <stdexcept>

class SymbolTableException :
	public std::invalid_argument
{
public:
	uint16_t errorCode;
	Token token;

	SymbolTableException();
	SymbolTableException(const std::string &_Message);
	SymbolTableException(const std::string &_Message, uint16_t errCode, int lineNo);
	SymbolTableException(char *str, uint16_t errCode, int lineNo);
	SymbolTableException(Token &tkn, uint16_t errCode);

	static SymbolTableException InvokeIncorrectTypesException(Symbol::Type type1, Symbol::Type type2, int lineNo);

	~SymbolTableException();
};

