#include "Symbol.h"

int Symbol::_nextSymbolID = 0;
Symbol Symbol::InvalidSymbol(true);

std::string Symbol::GetTypeName(Symbol::Type type)
{
	switch (type)
	{
	case Type::Void:
		return "Void";
	case Type::Bool:
		return "Bool";
	case Type::Int:
		return "Int";
	case Type::Decimal:
		return "Decimal";
	case Type::String:
		return "String";
	}
}

std::string Symbol::GetTokenTypeName(TokenType type)
{
	switch (type)
	{
	case TokenType::KWD_VOID:
		return "Void";
	case TokenType::KWD_BOOL:
		return "Bool";
	case TokenType::KWD_INT:
		return "Int";
	case TokenType::KWD_DECIMAL:
		return "Decimal";
	case TokenType::KWD_STRING:
		return "String";
	}
}

Symbol::Type Symbol::TokenTypeToSymbolType(TokenType type)
{
	switch (type)
	{
	case KWD_INT:
		return Symbol::Type::Int;
	case KWD_BOOL:
		return Symbol::Type::Bool;
	case KWD_DECIMAL:
		return Symbol::Type::Decimal;
	case KWD_STRING:
		return Symbol::Type::String;
	case KWD_VOID:
		return Symbol::Type::Void;
	}
}

Symbol::Symbol()
{
}

Symbol::Symbol(bool invalid)
{
	_invalid = invalid;
}

Symbol::Symbol(SymbolType st, Type t, Token i)
{
	if (i.type != TokenType::IDENT)
		throw "Incorrect TokenType encountered while constructing symbol table";

	symbolType = st;
	type = t;
	name = i.value.GetStringValue();
	lineNo = i.line;
	ID = _nextSymbolID++;
}


Symbol::~Symbol()
{
}

bool Symbol::operator==(Symbol & s)
{
	if (s._invalid == true && _invalid == true)
		return true;

	return s.symbolType == symbolType && s.type == type && s.name == name;
}
