#include "SymbolTable.h"
#include "SymbolTableException.h"
#include "SemanticErrors.h"

SymbolTable::SymbolTable()
{
}

SymbolTable::SymbolTable(SymbolTable * p) : parent(p)
{
}

void SymbolTable::AddSymbol(Symbol symbol)
{
	symbols.push_back(symbol);
}

void SymbolTable::AddSymbol(Symbol symbol, void * node)
{
	symbols.push_back(symbol);
	functionNodes[symbol.name] = node;
}

void SymbolTable::AddConstant(Token c)
{
	constants.push_back(c);
}

SymbolTable & SymbolTable::AddScope()
{
	subScopes.push_back(SymbolTable(this));
	return subScopes.back();
}

Symbol & SymbolTable::ResolveSymbol(Token name)
{
	_ASSERT(name.type == TokenType::IDENT);

	for (auto & s : symbols)
	{
		if (s.name == name.value.GetStringValue())
			return s;
	}
	if (parent != nullptr)
		return parent->ResolveSymbol(name);
	else
		SemanticErrors::AddError(SemanticErrors::Error(1, name.line, { name.value.GetStringValue() }));

	return Symbol::InvalidSymbol;
}

void * SymbolTable::ResolveFunction(std::string name)
{
	if (functionNodes.find(name) != functionNodes.end())
		return functionNodes[name];
	else
	{
		if (parent != nullptr)
			return parent->ResolveFunction(name);
		else
			return nullptr;
	}
}


SymbolTable::~SymbolTable()
{
}
