#pragma once
#include <vector>

namespace SemanticErrors
{
	struct Error
	{
		unsigned int errorCode;
		unsigned int lineNo;
		std::vector<std::string> args;
		Error() {}
		Error(unsigned int code, unsigned int line, std::vector<std::string> arg);
	};

	void InitSemanticErrors();
	void AddError(Error error);
	void PrintErrors(std::ostream &output, std::string fileName);
	bool HasErrors();

	std::string IntToString(int i);
}

