#include "../Syntax Analyser/Syntax Tree/Node.h"
#pragma once
class SemanticAnalyser
{
private:
	Node * rootNode;
public:
	SymbolTable globalSymbolTable;

	SemanticAnalyser(Node * root);
	~SemanticAnalyser();

	bool Verify();
};

