#pragma once
#include "../Token.h"

class Symbol
{
private:
	static int _nextSymbolID;
	bool _invalid = false;

	Symbol(bool invalid);
public:
	static Symbol InvalidSymbol;

	enum SymbolType { Variable, Function };
	enum Type { Void, Bool, Int, Decimal, String };

	static std::string GetTypeName(Symbol::Type type);
	static std::string GetTokenTypeName(TokenType type);
	static Type TokenTypeToSymbolType(TokenType type);
	
	SymbolType symbolType;
	Type type;
	std::string name;
	int lineNo;
	int ID;

	Symbol();
	Symbol(SymbolType st, Type t, Token i);
	~Symbol();

	bool operator==(Symbol & s);
};

