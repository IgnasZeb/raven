#pragma once
#include <vector>

typedef unsigned char byte;

class Operand
{
public:
	std::vector<byte> code;
	int symbolID = -1;

	Operand();
	Operand(int ID, std::vector<byte> codes);
	~Operand();
};

