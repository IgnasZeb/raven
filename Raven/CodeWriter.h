#pragma once
#include <fstream>
#include "Instruction.h"

class CodeWriter
{
private:
	std::ofstream file;
	std::vector<Instruction> _instructions;
public:
	CodeWriter(std::string fileName);

	void AddCommand(Instruction cmd);

	~CodeWriter();
};

