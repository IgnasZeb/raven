#pragma once
#include "Lexical Analyser/ScannerState.h"
#include <string>
#include <variant>
#include <unordered_map>

enum TokenType {
	INVALID = 0,
	OP_PLUS = 100,
	OP_POW,
	OP_MULT,
	OP_MOD,
	OP_MINUS,
	OP_DIV,
	OP_XOR,
	OP_EQ,
	OP_ASSIGN,
	OP_NOT_EQ,
	OP_G,
	OP_GE,
	OP_L,
	OP_LE,
	OP_AND,
	OP_BIN_AND,
	OP_OR,
	OP_BIN_OR,
	OP_BIN_NOT,
	OP_BRACK_O,
	OP_BRACK_C,
	OP_BRACE_O,
	OP_BRACE_C,
	OP_PARAN_O,
	OP_PARAN_C,
	OP_COLON,
	OP_SEMICOLON,
	OP_COMMA,
	IDENT,
	LIT_INT,
	LIT_DEC,
	LIT_STR,
	LIT_ESC_SEQ,
	_EOF,

	KWD_INT = 1000,
	KWD_BOOL,
	KWD_DECIMAL,
	KWD_STRING,
	KWD_VOID,
	KWD_FALSE,
	KWD_TRUE,
	KWD_PRINT,
	KWD_READ,
	KWD_WHILE,
	KWD_FOR,
	KWD_TO,
	KWD_AS,
	KWD_STEP,
	KWD_BREAK,
	KWD_CONTINUE,
	KWD_IF,
	KWD_ELIF,
	KWD_ELSE,
	KWD_RETURN,
	KWD_IN
};

struct TokenValue
{
	std::variant<std::string, int, float> value;

	std::string GetStringValue();

	void SetIntValue(std::string str);
	void SetFloatValue(std::string str);
	void SetByScannerState(std::string str, ScannerEnum::ScannerState state);
};

class Token
{
private:
	static std::unordered_map<TokenType, std::string> TokenNames;
public:
	static Token InvalidToken;

	Token();
	Token(TokenType tokenType, TokenValue tokenValue, long lineNo);
	~Token();

	TokenValue value;
	long line;
	TokenType type;

	std::string GetName();

	bool operator==(Token &other);
};