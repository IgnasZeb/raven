#include "Token.h"
#include <sstream>

std::unordered_map<TokenType, std::string> Token::TokenNames = {
	{ OP_PLUS, "OP_PLUS" },
	{ OP_POW, "OP_POW" },
	{ OP_MULT, "OP_MULT" },
	{ OP_MOD, "OP_MOD" },
	{ OP_MINUS, "OP_MINUS" },
	{ OP_DIV, "OP_DIV" },
	{ OP_XOR, "OP_XOR" },
	{ OP_EQ, "OP_EQ" },
	{ OP_ASSIGN, "OP_ASSIGN" },
	{ OP_NOT_EQ, "OP_NOT_EQ" },
	{ OP_G, "OP_G" },
	{ OP_GE, "OP_GE" },
	{ OP_L, "OP_L" },
	{ OP_LE, "OP_LE" },
	{ OP_AND, "OP_AND" },
	{ OP_BIN_AND, "OP_BIN_AND" },
	{ OP_OR, "OP_OR" },
	{ OP_BIN_OR, "OP_BIN_OR" },
	{ OP_BIN_NOT, "OP_BIN_NOT" },
	{ OP_BRACK_O, "OP_BRACK_O" },
	{ OP_BRACK_C, "OP_BRACK_C" },
	{ OP_BRACE_O, "OP_BRACE_O" },
	{ OP_BRACE_C, "OP_BRACE_C" },
	{ OP_PARAN_O, "OP_PARAN_O" },
	{ OP_PARAN_C, "OP_PARAN_C" },
	{ OP_COLON, "OP_COLON" },
	{ OP_SEMICOLON, "OP_SEMICOLON"},
	{ OP_COMMA, "OP_COMMA" },
	{ IDENT, "IDENT" },
	{ LIT_INT, "LIT_INT" },
	{ LIT_DEC, "LIT_DEC" },
	{ LIT_STR, "LIT_STR" },
	{ LIT_ESC_SEQ, "LIT_ESC_SEQ" },
	{ _EOF, "EOF"},
	{ KWD_INT, "KWD_INT" },
	{ KWD_BOOL, "KWD_BOOL" },
	{ KWD_DECIMAL, "KWD_DECIMAL" },
	{ KWD_STRING, "KWD_STRING" },
	{ KWD_VOID, "KWD_VOID" },
	{ KWD_FALSE, "KWD_FALSE" },
	{ KWD_TRUE, "KWD_TRUE" },
	{ KWD_PRINT, "KWD_PRINT" },
	{ KWD_READ, "KWD_READ" },
	{ KWD_WHILE, "KWD_WHILE" },
	{ KWD_FOR, "KWD_FOR" },
	{ KWD_TO, "KWD_TO" },
	{ KWD_AS, "KWD_AS" },
	{ KWD_STEP, "KWD_STEP" },
	{ KWD_BREAK, "KWD_BREAK" },
	{ KWD_CONTINUE, "KWD_CONTINUE" },
	{ KWD_IF, "KWD_IF" },
	{ KWD_ELIF, "KWD_ELIF" },
	{ KWD_ELSE, "KWD_ELSE" },
	{ KWD_RETURN, "KWD_RETURN" },
	{ KWD_IN, "KWD_IN" }
};

Token Token::InvalidToken(TokenType::INVALID, { "" }, -1);

Token::Token()
{
}

Token::Token(TokenType tokenType, TokenValue tokenValue, long lineNo)
{
	type = tokenType;
	value = tokenValue;
	line = lineNo;
}

Token::~Token()
{
}

std::string Token::GetName()
{
	return TokenNames[type];
}

bool Token::operator==(Token & other)
{
	return line == other.line && type == other.type && value.GetStringValue() == other.value.GetStringValue();
}

std::string TokenValue::GetStringValue()
{
	std::stringstream stream;
	std::string ret;
	if (std::holds_alternative<std::string>(value))
		return std::get<std::string>(value);
	else if (std::holds_alternative<int>(value))
		stream << std::get<int>(value);
	else if (std::holds_alternative<float>(value))
		stream << std::get<float>(value);

	stream >> ret;
	return ret;
}

void TokenValue::SetIntValue(std::string str)
{
	value = atoi(str.c_str());
}

void TokenValue::SetFloatValue(std::string str)
{
	value = (float)atof(str.c_str());
}

void TokenValue::SetByScannerState(std::string str, ScannerEnum::ScannerState state)
{
	switch (state)
	{
	case ScannerEnum::ScannerState::LIT_DEC:
		SetFloatValue(str);
		break;
	case ScannerEnum::ScannerState::LIT_INT:
		SetIntValue(str);
		break;
	case ScannerEnum::ScannerState::IDENT:
	case ScannerEnum::ScannerState::LIT_STR:
		value = str;
		break;
	default:
		value = str;
	}
}
