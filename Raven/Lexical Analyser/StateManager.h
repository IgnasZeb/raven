#pragma once
#include <unordered_map>
#include "Scanner.h"

namespace StateManager
{
	ScannerEnum::ScannerState StartStateGetNextState(char currChar);
	ScannerEnum::ScannerState MinusStateGetNextState(char currChar);
	ScannerEnum::ScannerState AsteriskStateGetNextState(char currChar);
	ScannerEnum::ScannerState EqualsStateGetNextState(char currChar);
	ScannerEnum::ScannerState ExclamationStateGetNextState(char currChar);
	ScannerEnum::ScannerState GStateGetNextState(char currChar);
	ScannerEnum::ScannerState LStateGetNextState(char currChar);
	ScannerEnum::ScannerState AndStateGetNextState(char currChar);
	ScannerEnum::ScannerState OrStateGetNextState(char currChar);
	ScannerEnum::ScannerState DivStateGetnextState(char currChar);
	ScannerEnum::ScannerState LCStateGetnextState(char currChar);
	ScannerEnum::ScannerState BCStateGetnextState(char currChar);
	ScannerEnum::ScannerState BCEndStateGetnextState(char currChar);

	ScannerEnum::ScannerState IdentStateGetNextState(char currChar);
	ScannerEnum::ScannerState IntegerStateGetNextState(char currChar);
	ScannerEnum::ScannerState DecimalStateGetNextState(char currChar);
	ScannerEnum::ScannerState DecimalENotStateGetNextState(char currChar);
	ScannerEnum::ScannerState DecimalENotBegStateGetNextState(char currChar);
	ScannerEnum::ScannerState StringStateGetNextState(char currChar);
	ScannerEnum::ScannerState LitEscSeqStateGetNextState(char currChar, std::string & currString);

	void ReplaceEscSeq(std::string & string, const char * escChar);
}
