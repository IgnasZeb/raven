#pragma once

namespace ScannerEnum
{
	enum ScannerState {
		START,
		ERROR,
		FINISH,
		ASTERISK,
		EQUALS,
		EXCLAMATION,
		G,
		L,
		AND,
		OR,
		MINUS,
		LIT_ESC_SEQ,
		DIV,
		LC,
		BC,
		BC_END,
		IDENT_BEGIN,
		INTEGER,
		DECIMAL,
		DECIMAL_E_NOT,
		DECIMAL_E_NOT_BEG,
		STRING,

		OP_PLUS = 100,
		OP_POW,
		OP_MULT,
		OP_MOD,
		OP_MINUS,
		OP_DIV,
		OP_XOR,
		OP_EQ,
		OP_ASSIGN,
		OP_NOT_EQ,
		OP_G,
		OP_GE,
		OP_L,
		OP_LE,
		OP_AND,
		OP_BIN_AND,
		OP_OR,
		OP_BIN_OR,
		OP_BIN_NOT,
		OP_BRACK_O,
		OP_BRACK_C,
		OP_BRACE_O,
		OP_BRACE_C,
		OP_PARAN_O,
		OP_PARAN_C,
		OP_COLON,
		OP_SEMICOLON,
		OP_COMMA,
		IDENT,
		LIT_INT,
		LIT_DEC,
		LIT_STR,
	};
}