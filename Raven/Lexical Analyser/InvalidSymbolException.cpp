#include "InvalidSymbolException.h"

InvalidSymbolException::InvalidSymbolException() : invalid_argument("")
{
}

InvalidSymbolException::InvalidSymbolException(const std::string & _Message) : invalid_argument(_Message)
{
	errorCode = 0;
}

InvalidSymbolException::InvalidSymbolException(const std::string & _Message, uint16_t errCode) : invalid_argument(_Message), errorCode(errCode)
{
}

InvalidSymbolException::~InvalidSymbolException()
{
}
