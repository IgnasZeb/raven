#include "Scanner.h"
#include "StateManager.h"
#include "InvalidSymbolException.h"

using namespace ScannerEnum;

Scanner::Scanner(const wchar_t * filePath) : _reader(filePath, std::ios::binary, 1024)
{
}

Scanner::Scanner(const char * filePath) : _reader(filePath, std::ios::binary, 1024)
{
}

char Scanner::ReadNextChar()
{
	char nextChar;
	if (_reader.eof())
	{
		_currentTokenStr += '!';
		return 0x03;
	}

	do
	{
		_reader.read(&nextChar, 1);
	} while (nextChar == 0x0D);

	if (nextChar == 0x0A)
		_currentLine++;
	_currentTokenStr += nextChar;

	return nextChar;
}

std::vector<Token>& Scanner::ParseFile()
{
	bool readNext = true;
	while (_state != ScannerEnum::ScannerState::FINISH)
	{
		if (readNext && _state < 100)
			_currentSymbol = ReadNextChar();
		readNext = true;

		try
		{
			switch (_state)
			{
			case ScannerEnum::ScannerState::START:
				_currentTokenStr.clear();
				_currentTokenStr += _currentSymbol;
				_currentToken = Token();
				_currentToken.line = _currentLine;
				_state = StateManager::StartStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::MINUS:
				_state = StateManager::MinusStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::ASTERISK:
				_state = StateManager::AsteriskStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::EQUALS:
				_state = StateManager::EqualsStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::EXCLAMATION:
				_state = StateManager::ExclamationStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::G:
				_state = StateManager::GStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::L:
				_state = StateManager::LStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::AND:
				_state = StateManager::AndStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::OR:
				_state = StateManager::OrStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::DIV:
				_state = StateManager::DivStateGetnextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::LC:
				_state = StateManager::LCStateGetnextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::BC:
				_state = StateManager::BCStateGetnextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::BC_END:
				_state = StateManager::BCEndStateGetnextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::IDENT_BEGIN:
				_state = StateManager::IdentStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::INTEGER:
				_state = StateManager::IntegerStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::DECIMAL:
				_state = StateManager::DecimalStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::DECIMAL_E_NOT_BEG:
				_state = StateManager::DecimalENotBegStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::DECIMAL_E_NOT:
				_state = StateManager::DecimalENotStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::STRING:
				_state = StateManager::StringStateGetNextState(_currentSymbol);
				break;
			case ScannerEnum::ScannerState::LIT_ESC_SEQ:
				_state = StateManager::LitEscSeqStateGetNextState(_currentSymbol, _currentTokenStr);
				break;

			case ScannerEnum::ScannerState::LIT_STR:
				readNext = false;
				_currentTokenStr = _currentTokenStr.substr(1);
			case ScannerEnum::ScannerState::LIT_DEC:
			case ScannerEnum::ScannerState::LIT_INT:
			case ScannerEnum::ScannerState::IDENT:
			case ScannerEnum::ScannerState::OP_MULT:
			case ScannerEnum::ScannerState::OP_ASSIGN:
			case ScannerEnum::ScannerState::OP_G:
			case ScannerEnum::ScannerState::OP_L:
			case ScannerEnum::ScannerState::OP_BIN_AND:
			case ScannerEnum::ScannerState::OP_BIN_OR:
			case ScannerEnum::ScannerState::OP_MINUS:
			case ScannerEnum::ScannerState::OP_DIV:
				readNext = !readNext;
			case ScannerEnum::ScannerState::OP_PLUS:
			case ScannerEnum::ScannerState::OP_POW:
			case ScannerEnum::ScannerState::OP_MOD:
			case ScannerEnum::ScannerState::OP_XOR:
			case ScannerEnum::ScannerState::OP_EQ:
			case ScannerEnum::ScannerState::OP_NOT_EQ:
			case ScannerEnum::ScannerState::OP_GE:
			case ScannerEnum::ScannerState::OP_LE:
			case ScannerEnum::ScannerState::OP_AND:
			case ScannerEnum::ScannerState::OP_OR:
			case ScannerEnum::ScannerState::OP_BIN_NOT:
			case ScannerEnum::ScannerState::OP_BRACK_O:
			case ScannerEnum::ScannerState::OP_BRACK_C:
			case ScannerEnum::ScannerState::OP_BRACE_O:
			case ScannerEnum::ScannerState::OP_BRACE_C:
			case ScannerEnum::ScannerState::OP_PARAN_O:
			case ScannerEnum::ScannerState::OP_PARAN_C:
			case ScannerEnum::ScannerState::OP_COLON:
			case ScannerEnum::ScannerState::OP_SEMICOLON:
			case ScannerEnum::ScannerState::OP_COMMA:
				if (_currentTokenStr.length() == 1)
					_currentToken.value.SetByScannerState(_currentTokenStr.substr(0, _currentTokenStr.length()), _state);
				else
					_currentToken.value.SetByScannerState(_currentTokenStr.substr(0, _currentTokenStr.length() - 1), _state);
				_currentToken.type = (TokenType)_state;
				tokens.push_back(_currentToken);
				_state = START;
				break;
			case ScannerEnum::ScannerState::FINISH:
				_currentToken.type = TokenType::_EOF;
				tokens.push_back(_currentToken);
				break;
			}
		}
		catch (const InvalidSymbolException& ex)
		{
			if (strlen(ex.what()) == 0)
			{
				char errorBuffer[_MAX_PATH+100];
				sprintf_s(errorBuffer, _MAX_PATH+100, "%s(%lu) : error L001: Invalid symbol '%c'", _reader.GetFilePath().c_str(), _currentLine, _currentSymbol);
				throw InvalidSymbolException(errorBuffer);
			}
			else
			{
				char errorBuffer[_MAX_PATH+500];
				sprintf_s(errorBuffer, _MAX_PATH+500, "%s(%lu) : error L%03u: %s", _reader.GetFilePath().c_str(), _currentLine, ex.errorCode, ex.what());
				throw InvalidSymbolException(errorBuffer);
			}
		}
		//_currentToken.value = "";
	}
	_currentToken.type = TokenType::_EOF;
	_currentToken.line = _currentLine;
	tokens.push_back(_currentToken);

	return tokens;
}
