#include "LexicalAnalyser.h"
#include "KeywordParser.h"

std::vector<Token>& LexicalAnalyser::Parse()
{
	tokens = _scanner.ParseFile();
	tokens = KeywordParser::ParseKeywords(tokens);
	return tokens;
}

LexicalAnalyser::LexicalAnalyser(const wchar_t * filePath) : _scanner(filePath)
{
}

LexicalAnalyser::LexicalAnalyser(const char * filePath) : _scanner(filePath)
{
}


LexicalAnalyser::~LexicalAnalyser()
{
}
