#pragma once
#include <fstream>
#include <string>
#include <filesystem>

class BufferReader :
	public std::ifstream
{
public:
	BufferReader(int buffSize);
	BufferReader(const wchar_t * filePath, int mode, int buffSize);
	BufferReader(const char * filePath, int mode, int buffSize);
	~BufferReader();

	void read(char * buff, std::streamsize size);
	bool eof();

	std::string GetFilePath();
private:
	char * _buffer;
	std::streamsize _bufferPointer;
	std::streamsize _bufferSize;
	std::streamsize _bufferLength;
	std::filesystem::path _filePath;
};

