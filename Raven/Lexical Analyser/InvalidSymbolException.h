#pragma once
#include <stdexcept>

class InvalidSymbolException :
	public std::invalid_argument
{
public:
	uint16_t errorCode;

	InvalidSymbolException();
	InvalidSymbolException(const std::string &_Message);
	InvalidSymbolException(const std::string &_Message, uint16_t errCode);
	~InvalidSymbolException();
};

