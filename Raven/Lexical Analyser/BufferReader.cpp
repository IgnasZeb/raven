#include "BufferReader.h"

BufferReader::BufferReader(int buffSize)
{
	_ASSERT(buffSize > 0);

	_buffer = new char[buffSize];
	_bufferSize = buffSize;
	_bufferLength = 0;
	_bufferPointer = 0;
}

BufferReader::BufferReader(const wchar_t * filePath, int mode, int buffSize) : std::ifstream(filePath, mode)
{
	_ASSERT(buffSize > 0);

	_buffer = new char[buffSize];
	_bufferSize = buffSize;
	_bufferLength = 0;
	_bufferPointer = 0;

	_filePath = filePath;
}

BufferReader::BufferReader(const char * filePath, int mode, int buffSize) : std::ifstream(filePath, mode)
{
	_ASSERT(buffSize > 0);

	_buffer = new char[buffSize];
	_bufferSize = buffSize;
	_bufferLength = 0;
	_bufferPointer = 0;

	_filePath = filePath;
}

BufferReader::~BufferReader()
{
	delete[] _buffer;
	_buffer = 0;
	_bufferSize = 0;
	_bufferLength = 0;
	_bufferPointer = 0;
}

void BufferReader::read(char * buff, std::streamsize size)
{
	std::streamsize buffPointer = 0;
	while (size > 0 && !eof())
	{
		if (_bufferLength - _bufferPointer > size)
		{
			memcpy_s(buff + buffPointer, (rsize_t)size, _buffer + _bufferPointer, (rsize_t)size);
			_bufferPointer += size;
			buffPointer += size;
			size -= size;
		}
		else if(_bufferLength != 0)
		{
			memcpy_s(buff + buffPointer, (rsize_t)(_bufferLength - _bufferPointer), _buffer + _bufferPointer, (rsize_t)(_bufferLength - _bufferPointer));
			size -= _bufferLength - _bufferPointer;
			buffPointer += _bufferLength - _bufferPointer;
			_bufferPointer = _bufferLength;
		}

		if (_bufferPointer == _bufferLength && !eof())
		{
			std::ifstream::read(_buffer, _bufferSize);
			_bufferLength = gcount();
			_bufferPointer = 0;
		}
	}
}

bool BufferReader::eof()
{
	return _bufferPointer == _bufferLength && std::ifstream::eof();
}

std::string BufferReader::GetFilePath()
{
	return std::filesystem::absolute(_filePath).generic_string();
}
