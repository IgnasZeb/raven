#pragma once
#include "../Token.h"
#include <vector>

namespace KeywordParser
{
	std::vector<Token> &ParseKeywords(std::vector<Token> &tokens);
}
