#include "StateManager.h"
#include "InvalidSymbolException.h"
#include <stdexcept>

namespace StateManager
{
	namespace
	{
		static std::unordered_map<char, ScannerEnum::ScannerState> startStateMap({
			{0x03, ScannerEnum::ScannerState::FINISH},
			{' ', ScannerEnum::ScannerState::START},
			{0x09, ScannerEnum::ScannerState::START},
			{0x0A, ScannerEnum::ScannerState::START},
			{'+', ScannerEnum::ScannerState::OP_PLUS},
			{'-', ScannerEnum::ScannerState::MINUS},
			{'/', ScannerEnum::ScannerState::DIV},
			{'*', ScannerEnum::ScannerState::ASTERISK},
			{'%', ScannerEnum::ScannerState::OP_MOD},
			{'^', ScannerEnum::ScannerState::OP_XOR},
			{'=', ScannerEnum::ScannerState::EQUALS},
			{'!', ScannerEnum::ScannerState::EXCLAMATION},
			{'>', ScannerEnum::ScannerState::G},
			{'<', ScannerEnum::ScannerState::L},
			{'&', ScannerEnum::ScannerState::AND},
			{'|', ScannerEnum::ScannerState::OR},
			{'~', ScannerEnum::ScannerState::OP_BIN_NOT},
			{'[', ScannerEnum::ScannerState::OP_BRACK_O},
			{']', ScannerEnum::ScannerState::OP_BRACK_C},
			{'{', ScannerEnum::ScannerState::OP_BRACE_O},
			{'}', ScannerEnum::ScannerState::OP_BRACE_C},
			{'(', ScannerEnum::ScannerState::OP_PARAN_O},
			{')', ScannerEnum::ScannerState::OP_PARAN_C},
			{':', ScannerEnum::ScannerState::OP_COLON},
			{';', ScannerEnum::ScannerState::OP_SEMICOLON},
			{',', ScannerEnum::ScannerState::OP_COMMA},
			{'"', ScannerEnum::ScannerState::STRING},

			{'0', ScannerEnum::ScannerState::INTEGER},
			{'1', ScannerEnum::ScannerState::INTEGER},
			{'2', ScannerEnum::ScannerState::INTEGER},
			{'3', ScannerEnum::ScannerState::INTEGER},
			{'4', ScannerEnum::ScannerState::INTEGER},
			{'5', ScannerEnum::ScannerState::INTEGER},
			{'6', ScannerEnum::ScannerState::INTEGER},
			{'7', ScannerEnum::ScannerState::INTEGER},
			{'8', ScannerEnum::ScannerState::INTEGER},
			{'9', ScannerEnum::ScannerState::INTEGER},

			{'_', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'A', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'a', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'B', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'b', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'C', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'c', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'D', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'d', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'E', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'e', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'F', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'f', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'G', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'g', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'H', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'h', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'I', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'i', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'J', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'j', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'K', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'k', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'L', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'l', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'M', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'m', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'N', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'n', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'O', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'o', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'P', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'p', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'Q', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'q', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'R', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'r', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'S', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'s', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'T', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'t', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'U', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'u', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'V', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'v', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'W', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'w', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'X', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'x', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'Y', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'y', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'Z', ScannerEnum::ScannerState::IDENT_BEGIN},
			{'z', ScannerEnum::ScannerState::IDENT_BEGIN}
		});
	}

	ScannerEnum::ScannerState StartStateGetNextState(char currChar)
	{
		if (startStateMap.find(currChar) == startStateMap.end())
			throw InvalidSymbolException();

		return startStateMap[currChar];
	}

	ScannerEnum::ScannerState MinusStateGetNextState(char currChar)
	{
		if (isdigit(currChar))
			return ScannerEnum::ScannerState::LIT_INT;
		else
			return ScannerEnum::ScannerState::OP_MINUS;
	}

	ScannerEnum::ScannerState AsteriskStateGetNextState(char currChar)
	{
		if (currChar == '*')
			return ScannerEnum::ScannerState::OP_POW;
		else
			return ScannerEnum::ScannerState::OP_MULT;
	}

	ScannerEnum::ScannerState EqualsStateGetNextState(char currChar)
	{
		if (currChar == '=')
			return ScannerEnum::ScannerState::OP_EQ;
		else
			return ScannerEnum::ScannerState::OP_ASSIGN;
	}
	ScannerEnum::ScannerState ExclamationStateGetNextState(char currChar)
	{
		if (currChar == '=')
			return ScannerEnum::ScannerState::OP_NOT_EQ;
		else
			throw InvalidSymbolException("Invalid symbol '!'", 2);
	}
	ScannerEnum::ScannerState GStateGetNextState(char currChar)
	{
		if (currChar == '=')
			return ScannerEnum::ScannerState::OP_GE;
		else
			return ScannerEnum::ScannerState::OP_G;
	}
	ScannerEnum::ScannerState LStateGetNextState(char currChar)
	{
		if (currChar == '=')
			return ScannerEnum::ScannerState::OP_LE;
		else
			return ScannerEnum::ScannerState::OP_L;
	}
	ScannerEnum::ScannerState AndStateGetNextState(char currChar)
	{
		if (currChar == '&')
			return ScannerEnum::ScannerState::OP_AND;
		else
			return ScannerEnum::ScannerState::OP_BIN_AND;
	}
	ScannerEnum::ScannerState OrStateGetNextState(char currChar)
	{
		if (currChar == '|')
			return ScannerEnum::ScannerState::OP_OR;
		else
			return ScannerEnum::ScannerState::OP_BIN_OR;
	}
	ScannerEnum::ScannerState DivStateGetnextState(char currChar)
	{
		if (currChar == '/')
			return ScannerEnum::ScannerState::LC;
		else if (currChar == '*')
			return ScannerEnum::ScannerState::BC;
		else
			return ScannerEnum::ScannerState::OP_DIV;
	}
	ScannerEnum::ScannerState LCStateGetnextState(char currChar)
	{
		if (currChar == 0x0A)
			return ScannerEnum::ScannerState::START;
		else
			return ScannerEnum::ScannerState::LC;
	}
	ScannerEnum::ScannerState BCStateGetnextState(char currChar)
	{
		if (currChar == '*')
			return ScannerEnum::ScannerState::BC_END;
		else if (currChar == 0x03)
			throw InvalidSymbolException("Closing block comment tag expected before the end of file", 3);
		else
			return ScannerEnum::ScannerState::BC;
	}
	ScannerEnum::ScannerState BCEndStateGetnextState(char currChar)
	{
		if (currChar == '/')
			return ScannerEnum::ScannerState::START;
		else if (currChar == '*')
			return ScannerEnum::ScannerState::BC_END;
		else if (currChar == 0x03)
			throw InvalidSymbolException("Closing block comment tag expected before the end of file", 3);
		else
			return ScannerEnum::ScannerState::BC;
	}
	ScannerEnum::ScannerState IdentStateGetNextState(char currChar)
	{
		if (currChar == '_' || isalnum(currChar))
			return ScannerEnum::ScannerState::IDENT_BEGIN;
		else
			return ScannerEnum::ScannerState::IDENT;
	}
	ScannerEnum::ScannerState IntegerStateGetNextState(char currChar)
	{
		if (currChar == '.')
			return ScannerEnum::ScannerState::DECIMAL;
		else if (isdigit(currChar))
			return ScannerEnum::ScannerState::INTEGER;
		else if (isalpha(currChar))
			throw InvalidSymbolException();
		else
			return ScannerEnum::ScannerState::LIT_INT;
	}
	ScannerEnum::ScannerState DecimalStateGetNextState(char currChar)
	{
		if (isdigit(currChar))
			return ScannerEnum::ScannerState::DECIMAL;
		else if (currChar == 'e')
			return ScannerEnum::ScannerState::DECIMAL_E_NOT_BEG;
		else if (isalpha(currChar))
			throw InvalidSymbolException();
		else
			return ScannerEnum::ScannerState::LIT_DEC;
	}
	ScannerEnum::ScannerState DecimalENotStateGetNextState(char currChar)
	{
		if (isdigit(currChar))
			return ScannerEnum::ScannerState::DECIMAL_E_NOT;
		else if (isalpha(currChar))
			throw InvalidSymbolException();
		else
			return ScannerEnum::ScannerState::LIT_DEC;
	}
	ScannerEnum::ScannerState DecimalENotBegStateGetNextState(char currChar)
	{
		if (isdigit(currChar) || currChar == '-')
			return ScannerEnum::ScannerState::DECIMAL_E_NOT;
		else
			throw InvalidSymbolException("Illegal decimal scientific notation", 4);
	}
	ScannerEnum::ScannerState StringStateGetNextState(char currChar)
	{
		if (currChar == '"')
			return ScannerEnum::ScannerState::LIT_STR;
		else if (currChar == '\\')
			return ScannerEnum::ScannerState::LIT_ESC_SEQ;
		else if (currChar == 0x0A)
			throw InvalidSymbolException("Expected symbol \" before newline", 5);
		else if(currChar == 0x03)
			throw InvalidSymbolException("Expected symbol \" before the end of file", 6);
		else
			return ScannerEnum::ScannerState::STRING;
	}
	ScannerEnum::ScannerState LitEscSeqStateGetNextState(char currChar, std::string & currString)
	{
		switch (currChar)
		{
		case 'n':
			ReplaceEscSeq(currString, "\n");
			break;
		case '"':
			ReplaceEscSeq(currString, "\"");
			break;
		case '\\':
			ReplaceEscSeq(currString, "\\");
			break;
		case 0x03:
			throw InvalidSymbolException("Escape sequence expected, found end of file", 7);
		default:
			char errorBuffer[100];
			sprintf_s(errorBuffer, 100, "Illegal escape sequence '\\%c'", currChar);
			throw InvalidSymbolException(errorBuffer, 8);
		}
		return ScannerEnum::ScannerState::STRING;
	}
	void ReplaceEscSeq(std::string & string, const char * escChar)
	{
		string.erase(string.end() - 2, string.end());
		string += escChar;
	}
}
