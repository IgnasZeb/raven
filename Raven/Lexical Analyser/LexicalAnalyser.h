#pragma once
#include "Scanner.h"
#include <vector>

class LexicalAnalyser
{
private:
	Scanner _scanner;
public:
	std::vector<Token> tokens;

	std::vector<Token>& Parse();

	LexicalAnalyser(const wchar_t * filePath);
	LexicalAnalyser(const char * filePath);
	~LexicalAnalyser();
};

