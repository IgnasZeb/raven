#pragma once
#include <vector>
#include "BufferReader.h"
#include "../Token.h"
#include "ScannerState.h"

class Scanner
{
private:
	ScannerEnum::ScannerState _state = ScannerEnum::ScannerState::START;
	long _currentLine = 1;
	char _currentSymbol = 0;
	std::string _currentTokenStr = "";
	Token _currentToken;

	BufferReader _reader;

	char ReadNextChar();
public:
	std::vector <Token> tokens;

	Scanner(const wchar_t * filePath);
	Scanner(const char * filePath);

	std::vector<Token>& ParseFile();
};