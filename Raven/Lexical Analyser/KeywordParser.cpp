#include "KeywordParser.h"
#include <unordered_map>

namespace KeywordParser
{
	namespace
	{
		std::unordered_map<std::string, TokenType> keywordMap = {
			{"int",	TokenType::KWD_INT},
			{"bool", TokenType::KWD_BOOL},
			{"decimal", TokenType::KWD_DECIMAL},
			{"string", TokenType::KWD_STRING},
			{"void", TokenType::KWD_VOID},
			{"false", TokenType::KWD_FALSE},
			{"true", TokenType::KWD_TRUE},
			{"print", TokenType::KWD_PRINT},
			{"read", TokenType::KWD_READ},
			{"while", TokenType::KWD_WHILE},
			{"for", TokenType::KWD_FOR},
			{"to", TokenType::KWD_TO},
			{"as", TokenType::KWD_AS},
			{"step", TokenType::KWD_STEP},
			{"break", TokenType::KWD_BREAK},
			{"continue", TokenType::KWD_CONTINUE},
			{"if", TokenType::KWD_IF},
			{"elif", TokenType::KWD_ELIF},
			{"else", TokenType::KWD_ELSE},
			{"return", TokenType::KWD_RETURN},
			{"in", TokenType::KWD_IN}
		};
	}

	std::vector<Token> &ParseKeywords(std::vector<Token> &tokens)
	{
		for (int i = 0; i < tokens.size(); i++)
		{
			if (tokens[i].type == TokenType::IDENT)
			{
				if (keywordMap.find(std::get<std::string>(tokens[i].value.value)) != keywordMap.end())
				{
					tokens[i].type = keywordMap[std::get<std::string>(tokens[i].value.value)];
				}
			}
		}
		return tokens;
	}
}
