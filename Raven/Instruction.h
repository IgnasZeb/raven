#pragma once
#include "Operand.h"

class Instruction
{
public:
	byte opCode;
	std::vector<Operand> operands;

	Instruction();
	Instruction(byte op, std::vector<Operand> &opers);
	~Instruction();
};