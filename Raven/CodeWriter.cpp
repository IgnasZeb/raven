#include "CodeWriter.h"

CodeWriter::CodeWriter(std::string fileName) : file(fileName, std::ios::binary)
{
}

void CodeWriter::AddCommand(Instruction cmd)
{
	_instructions.push_back(cmd);
}

CodeWriter::~CodeWriter()
{
}
