#include <iostream>
#include <iomanip>

#include "Lexical Analyser/LexicalAnalyser.h"
#include "Lexical Analyser/InvalidSymbolException.h"
#include "Syntax Analyser/LRParser.h"
#include "Syntax Analyser/InvalidTokenException.h"
#include "Semantic Analyser/SymbolTableException.h"
#include "Semantic Analyser/SemanticAnalyser.h"
#include "Semantic Analyser/SemanticErrors.h"


void PrintTokens(std::vector<Token> &tokens, std::ostream &stream)
{
	stream << std::left << std::setw(9) << "Line No." << std::left << std::setw(20) << "|  Type" << "|  Value\n";
	stream << "-----------------------------------------------\n";
	for (auto & t : tokens)
	{
		char lineBuffer[10];
		_itoa_s(t.line, lineBuffer, 10, 10);
		stream << std::left << std::setw(9) << lineBuffer << "| " << std::left << std::setw(18) << t.GetName() << "| " << std::left << std::setw(29) << t.value.GetStringValue() << "\n";
	}
	stream << "\n\n";
}

int main(int argc, char *argv[])
{
	if (argc == 1)
	{
		std::cout << "Please specify file to compile!";
		return 0;
	}

	try
	{
		LexicalAnalyser lexer(argv[1]);
		std::vector<Token> tokens = lexer.Parse();
		PrintTokens(tokens, std::cout);

		LRParser parser(tokens);
		parser.Parse();
		parser.rootNode->get()->PrintTree(0);

		SemanticAnalyser semanticAnalyser(parser.rootNode);
		if (!semanticAnalyser.Verify())
			SemanticErrors::PrintErrors(std::cerr, argv[1]);
	}
	catch (InvalidSymbolException& ex)
	{
		std::cerr << ex.what();
	}
	catch (InvalidTokenException& ex)
	{
		if (strlen(ex.what()) == 0)
		{
			char errorBuffer[_MAX_PATH + 500];
			sprintf_s(errorBuffer, _MAX_PATH + 500, "%s(%lu) : error P%03u: Unexpected token %s", argv[1], ex.token.line, ex.errorCode, ex.token.value.GetStringValue().c_str());
			std::cerr << errorBuffer;
		}
		else
			std::cerr << ex.what();
	}
	catch (SymbolTableException &ex)
	{
		if (strlen(ex.what()) == 0)
		{
			char errorBuffer[_MAX_PATH + 500];
			sprintf_s(errorBuffer, _MAX_PATH + 500, "%s(%lu) : error N%03u: Unidentified identifier %s", argv[1], ex.token.line, ex.errorCode, ex.token.value.GetStringValue().c_str());
			std::cerr << errorBuffer;
		}
		else
		{
			char errorBuffer[_MAX_PATH + 500];
			sprintf_s(errorBuffer, _MAX_PATH + 500, "%s(%lu) : error N%03u: %s", argv[1], ex.token.line, ex.errorCode, ex.what());
			std::cerr << errorBuffer;
		}
	}

	return 0;
}