@echo off
if EXIST %2 (
    SET /p oldHash= < %2
) else SET oldHash=0
certutil -hashfile %1 | findstr /V ":" > %2
SET /p newHash= < %2

IF NOT %oldHash% == %newHash% (
    echo Grammar changed, regenerating LR table...
    "C:\Users\ignas\Documents\Visual Studio 2017\Projects\Raven\Debug\LRTableGenerator.exe" -i %1 -ocpp %3 -oh %4
) else (
    echo Grammar did not change, no need to regenerate LR table
)