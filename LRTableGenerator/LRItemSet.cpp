#include "LRItemSet.h"
#include <algorithm>

int LRItemSet::uid = 0;

LRItemSet::LRItemSet()
{
	stateUID = uid;
	uid++;
}


LRItemSet::~LRItemSet()
{
}


bool LRItemSet::InSet(LRItem &item)
{
	for (LRItem &i : items)
	{
		if (i == item)
			return true;
	}
	return false;
}

bool LRItemSet::CanBeMergedIntoCores(LRItemSet & other)
{
	std::vector<std::string> thisCores, otherCores;
	for (const LRItem &item : items)
	{
		if (item.kernel)
		{
			bool alreadyInVector = false;
			std::string hash = item.GetCoreHash();
			for (const std::string & h : thisCores)
			{
				if (hash == h)
				{
					alreadyInVector = true;
					break;
				}
			}
			if (!alreadyInVector)
				thisCores.push_back(hash);
		}
	}

	for (const LRItem &item : other.items)
	{
		if (item.kernel)
		{
			bool alreadyInVector = false;
			std::string hash = item.GetCoreHash();
			for (const std::string & h : otherCores)
			{
				if (hash == h)
				{
					alreadyInVector = true;
					break;
				}
			}
			if (!alreadyInVector)
				otherCores.push_back(hash);
		}
	}

	if (thisCores.size() != otherCores.size())
		return false;

	std::sort(thisCores.begin(), thisCores.end());
	std::sort(otherCores.begin(), otherCores.end());

	for (int i = 0; i < thisCores.size(); i++)
	{
		if (thisCores[i] != otherCores[i])
			return false;
	}

	return true;
}

bool LRItemSet::CanBeMergedIntoReduce(LRItemSet & other)
{
	for (const LRItem & item1 : items)
	{
		for (const LRItem & item2 : other.items)
		{
			if (item1.action.type == Action::Reduce && item2.action.type == Action::Reduce &&
				item1.action.lookahead == item2.action.lookahead && item1.action.stateNo != item2.action.stateNo)
				return false;
		}
	}

	return true;
}

bool LRItemSet::operator==(LRItemSet & other)
{
	if (items.size() != other.items.size())
		return false;

	for (const LRItem & i : items)
	{
		for (const LRItem & oi : other.items)
		{
			if (i.LHS.get().symbolName != i.LHS.get().symbolName)
				return false;
		}
	}
}
