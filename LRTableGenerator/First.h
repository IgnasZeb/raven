#pragma once
#include <map>
#include "NonTerminal.h"
#include "Terminal.h"

class First
{
private:
	std::map<std::string, NonTerminal> &_nonTerminals;
	std::map<std::string, Terminal> &_terminals;
	std::map<std::string, std::vector<SymbolRef>> _firsts;

	void CalculateFirst(SymbolRef symbolRef);
	bool FirstCalculated(SymbolRef symbolRef);

	bool FirstRule(SymbolRef symbol, Symbol &outSymbol);
	bool SecondRule(SymbolRef symbol, std::vector<SymbolRef> &symbols);
	bool ThirdRule(SymbolRef symbol, std::vector<SymbolRef> &symbols);
public:
	First(std::map<std::string, NonTerminal> &symbols, std::map<std::string, Terminal> &terminals);
	~First();

	std::map<std::string, std::vector<SymbolRef>> &CalculateFirsts();
};

