#include <exception>
#include "CMDUtils.h"


namespace CMDUtils
{
	namespace
	{
		template<class T>
		int GetArgPos(std::vector<std::pair<std::string, std::reference_wrapper<T>>> args, std::string argName)
		{
			for (int i = 0; i < args.size(); i++)
			{
				if (args[i].first == argName)
					return i;
			}

			return -1;
		}
	}

	int ParseArguments(int argc, char ** argv, SwitchArgs booleanArgs, ValueArgs stringArgs, bool notFoundException)
	{
		int argCount = 0;

		for (auto arg : booleanArgs)
		{
			if (GetArgPos<std::string>(stringArgs, arg.first) != -1)
				throw std::invalid_argument("Same argument in both booleanArgs and stringArgs vectors");
			arg.second.get() = false;
		}

		for (int i = 0; i < argc; i++)
		{
			std::string argName = argv[i];

			if (argName.find("--") == 0)
				argName = argName.erase(0, 2);
			else if (argName.find("-") == 0)
				argName = argName.erase(0, 1);
			else
				continue;

			int argPos = GetArgPos(booleanArgs, argName);

			if (argPos != -1)
			{
				booleanArgs[argPos].second.get() = true;
				argCount++;
			}
			else if((argPos = GetArgPos(stringArgs, argName)) != -1)
			{
				if (i + 1 == argc)
				{
					const char msgFmt[] = "Argument %s value required but not specified.";
					int len = _scprintf(msgFmt, argName);
					char *buffer = new char[len + 1];
					_snprintf_s(buffer, len + 1, len, msgFmt, argName.c_str());
					std::string exceptionMsg = buffer;
					delete[] buffer;
					throw std::invalid_argument(exceptionMsg);
				}

				std::string argValue = argv[++i];
				if (argValue[0] == '"' && *(argValue.end()-1) == '"')
				{
					argValue.erase(argValue.begin());
					argValue.erase(argValue.end()-1);
				}
				stringArgs[argPos].second.get() = argValue;
				argCount++;
			}
			else
			{
				if (notFoundException)
				{
					const char msgFmt[] = "Unknown argument %s specified.";
					int len = _scprintf(msgFmt, argName);
					char *buffer = new char[len+1];
					_snprintf_s(buffer, len + 1, len, msgFmt, argName.c_str());
					std::string exceptionMsg = buffer;
					delete[] buffer;
					throw std::invalid_argument(exceptionMsg);
				}
			}
		}

		return 0;
	}
}
