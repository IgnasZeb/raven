#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <map>
#include <string>

#include "LRItem.h"
#include "NonTerminal.h"

namespace CodeGenerator
{
	void GenerateCpp(std::string filePath, std::vector<std::pair<std::reference_wrapper<NonTerminal>, int>> RHS,
		std::map<int, std::map<std::string, Action>> actionTable,
		std::map<int, std::map<std::string, int>> gotoTable);

	void GenerateHeader(std::string filePath);
}