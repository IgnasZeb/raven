#include "LRItem.h"
#include "NonTerminal.h"

LRItem::LRItem(Rule r, SymbolRef symbol, std::vector<SymbolRef>& rhs, SymbolRef la, int pos, bool isKernel)
	: LHS(symbol), RHS(rhs), lookahead(la), position(pos), kernel(isKernel), rule(r)
{
}

LRItem::~LRItem()
{
}

std::string LRItem::GetCoreHash() const
{
	std::string hash = LHS.get().symbolName + " -> ";
	for (int i = 0; i < RHS.get().size(); i++)
	{
		if (i == position)
			hash += ".";
		hash += RHS.get()[i].get().symbolName;
	}
	return hash;
}

bool LRItem::operator==(LRItem & other)
{
	if (LHS.get().symbolName != other.LHS.get().symbolName ||
		lookahead.get().symbolName != other.lookahead.get().symbolName ||
		position != other.position)
		return false;

	if (RHS.get().size() != other.RHS.get().size())
		return false;
	for (int i = 0; i < RHS.get().size(); i++)
	{
		if (RHS.get()[i].get().symbolName != other.RHS.get()[i].get().symbolName)
			return false;
	}

	return true;
}
