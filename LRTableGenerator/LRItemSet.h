#pragma once

#include "LRItem.h"
#include <vector>
#include <list>

class LRItemSet
{
private:
	static int uid;

public:
	LRItemSet();
	~LRItemSet();

	bool InSet(LRItem &item);
	bool CanBeMergedIntoCores(LRItemSet &other);
	bool CanBeMergedIntoReduce(LRItemSet &other);

	std::list<LRItem> items;
	bool completed = false;
	int stateNo;
	int stateUID;

	bool operator==(LRItemSet &other);
};

