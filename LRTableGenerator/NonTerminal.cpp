#include "NonTerminal.h"



NonTerminal::NonTerminal(std::string name) : Symbol(name)
{
	production.resize(0);
	type = SymbolType::NonTerminal;
}

NonTerminal::NonTerminal()
{
}

NonTerminal::~NonTerminal()
{
}
