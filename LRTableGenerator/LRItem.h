#pragma once
#include "Symbol.h"
#include <vector>

struct Action {
	enum ActionType
	{
		Unset,
		Shift,
		Reduce,
		Goto
	};
	ActionType type = Unset;
	int stateNo = -1;
	std::string lookahead;
	int stateUID;

	Action() {}
	Action(ActionType t, int state) : type(t), stateNo(state) {}
};

struct Rule {
	std::string prodName;
	int prodNo;
	Rule(std::string name, int no) : prodName(name), prodNo(no) {}

	bool operator<(const Rule & other) const
	{
		if (prodName == other.prodName)
			return prodNo < other.prodNo;
		else
			return prodName.compare(other.prodName) < 0;
	}
};

class LRItem
{
public:
	LRItem(Rule r, SymbolRef symbol, std::vector<SymbolRef> &rhs, SymbolRef la, int pos, bool isKernel);
	~LRItem();

	SymbolRef LHS;
	std::reference_wrapper<std::vector<SymbolRef>> RHS;
	SymbolRef lookahead;
	int position;
	bool kernel;
	Action action;
	Rule rule;


	//DEBUG VARS
	int mergedFrom = -1;

	std::string GetCoreHash() const;

	bool operator==(LRItem &other);
};

