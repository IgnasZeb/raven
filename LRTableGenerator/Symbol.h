#pragma once
#include <string>

class Symbol
{
public:
	enum SymbolType { Terminal, NonTerminal, End };

	Symbol(std::string name);
	Symbol(std::string name, SymbolType _type);
	Symbol();
	~Symbol();

	std::string symbolName;
	SymbolType type;
};

typedef std::reference_wrapper<Symbol> SymbolRef;