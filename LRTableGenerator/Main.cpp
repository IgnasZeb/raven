#include "Parser.h"
#include "First.h"
#include "TableGenerator.h"
#include "CMDUtils.h"
#include "CodeGenerator.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>

int main(int argc, char **argv)
{
	std::string inputPath, outCppPath, outHPath;
	bool verbose;

	CMDUtils::SwitchArgs switches({ {"v", verbose}, 
									{"verbose", verbose} });

	CMDUtils::ValueArgs valueArgs({ {"i", inputPath},
									{"ocpp", outCppPath},
									{"oh", outHPath} });

	CMDUtils::ParseArguments(argc, argv, switches, valueArgs, false);

	//Parser parser("C:\\Users\\ignas\\Documents\\Univeras\\TM\\Grammar\\syntactical grammar.txt");
	Parser parser(inputPath.c_str());
	parser.Parse();
	First first(parser.nonTerminals, parser.terminals);
	auto firsts = first.CalculateFirsts();
	TableGenerator table(firsts, parser.nonTerminals, parser.firstSymbol);
	table.Generate();


	if (verbose)
	{
		for (const LRItemSet &set : table.doneList)
		{
			std::cout << "[" << set.stateNo << "]:\n";
			for (const LRItem & item : set.items)
			{
				std::stringstream ss;
				char buffer[10];
				_itoa_s(item.mergedFrom, buffer, 10, 10);
				ss << item.GetCoreHash() << ((item.kernel) ? " *" : "") << ((item.mergedFrom != -1) ? std::string("\tM") + buffer : "");
				std::cout << "\t" << std::left << std::setw(120) << ss.str() << std::setw(30) << item.lookahead.get().symbolName << std::setw(30) << item.action.lookahead
					<< ((item.action.type == Action::Shift) ? "s\t" : ((item.action.type == Action::Reduce) ? "r\t" : ((item.action.type == Action::Goto) ? "g\t" : "u\t"))) << item.action.stateNo << "\n";
			}
		}
	}

	std::map<int, std::map<std::string, Action>> actionTable;
	std::map<int, std::map<std::string, int>> gotoTable;

	for (const LRItemSet &set : table.doneList)
	{
		for (const LRItem &item : set.items)
		{
			switch (item.action.type)
			{
			case Action::Shift:
				if (actionTable[set.stateNo][item.action.lookahead].type == Action::Reduce)
					std::cout << "Encountered Shift/Reduce conflict in state " << set.stateNo << ": resolved as Shift\n";
				actionTable[set.stateNo][item.action.lookahead] = Action(Action::Shift, item.action.stateNo);
				break;
			case Action::Reduce:
				if (actionTable[set.stateNo][item.action.lookahead].type == Action::Shift)
					std::cout << "Encountered Shift/Reduce conflict in state " << set.stateNo << ": resolved as Shift\n";
				else if (actionTable[set.stateNo][item.action.lookahead].type == Action::Reduce && actionTable[set.stateNo][item.action.lookahead].stateNo != item.action.stateNo)
				{
					std::cout << "Encountered Reduce/Reduce conflict. Halting...\n";
					throw "Encountered Reduce/Reduce conflict. Halting...\n";
				}
				else
					actionTable[set.stateNo][item.action.lookahead] = Action(Action::Reduce, item.action.stateNo);
				break;
			case Action::Goto:
				gotoTable[set.stateNo][item.action.lookahead] = item.action.stateNo;
				break;
			}
		}
	}

	CodeGenerator::GenerateHeader(outHPath);
	CodeGenerator::GenerateCpp(outCppPath, table.ruleList, actionTable, gotoTable);

	return 0;
}