#pragma once
#include "Symbol.h"
#include <vector>

class NonTerminal :
	public Symbol
{
public:
	NonTerminal(std::string name);
	NonTerminal();
	~NonTerminal();

	std::vector< std::vector<SymbolRef> > production;
};

typedef std::shared_ptr<NonTerminal> SPNonTerminal;

