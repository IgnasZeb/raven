﻿#include "TableGenerator.h"
#include <iostream>


//For each item[A → α ⋅ B β, t] in S,
//	For each production B → γ in G,
//		For each token b in FIRST(βt),
//			Add[B → ⋅ γ, b] to S
void TableGenerator::GenerateClosureItems(LRItemSet & set)
{
	for (LRItem & item : set.items)
	{
		if (item.position < item.RHS.get().size())
		{
			Symbol &deltaItem = item.RHS.get()[item.position].get();
			if (deltaItem.type == Symbol::NonTerminal)
			{
				int prodNo = 0;
				for (std::vector<SymbolRef> & prod : _nonTerminals[deltaItem.symbolName].production)
				{
					if (item.position == item.RHS.get().size() - 1)
					{
						LRItem newItem(Rule(deltaItem.symbolName, prodNo), item.RHS.get()[item.position], prod, item.lookahead, 0, false);
						if (!set.InSet(newItem))
							set.items.push_back(newItem);
					}
					else
					{
						for (SymbolRef &first : _firsts[item.RHS.get()[item.position + 1].get().symbolName])
						{
							LRItem newItem(Rule(deltaItem.symbolName, prodNo), item.RHS.get()[item.position], prod, first, 0, false);
							if (!set.InSet(newItem))
							{
								set.items.push_back(newItem);
							}
						}
					}
					prodNo++;
				}
			}
		}
		else
		{
			item.action.type = Action::Reduce;
			item.action.lookahead = item.lookahead.get().symbolName;
		}
	}
}

void TableGenerator::MergeItemSets(LRItemSet & merge, LRItemSet & into, bool mergeNonkernels)
{
	for (LRItem & mi : merge.items)
	{
		if (mi.action.type == Action::Reduce)
		{
			bool alreadyInSet = false;
			for (LRItem & item : into.items)
			{
				if (mi == item && item.action.type == Action::Reduce && mi.action.stateNo == item.action.stateNo)
				{
					alreadyInSet = true;
					break;
				}
			}

			if (!alreadyInSet)
			{
				mi.mergedFrom = merge.stateUID;
				into.items.push_back(mi);
			}
		}
		else
		{
			if (mi.kernel || mergeNonkernels)
			{
				if (!into.InSet(mi))
				{
					mi.mergedFrom = merge.stateUID;
					into.items.push_back(mi);
				}
			}
		}
	}
}

void TableGenerator::SetCompletedInDoneList(int uid)
{
	for (LRItemSet & set : doneList)
	{
		if (set.stateUID == uid)
		{
			set.completed = true;
			break;
		}
	}
}

LRItemSet & TableGenerator::GetSetByUID(int uid)
{
	for (LRItemSet & item : doneList)
	{
		if (item.stateUID == uid)
			return item;
	}

	for (LRItemSet & item : incList)
	{
		if (item.stateUID == uid)
			return item;
	}

	throw "StateUID does not exist!";
}

TableGenerator::TableGenerator(std::map<std::string, std::vector<SymbolRef>> & firsts, std::map < std::string, NonTerminal> & nonTerminals, SymbolRef firstSymbol) :
	_firsts(firsts), _nonTerminals(nonTerminals), _firstSymbol(firstSymbol), _endSymbol("EOF", Symbol::End), _endSymbolRef(_endSymbol)
{
}


TableGenerator::~TableGenerator()
{
}

void TableGenerator::Generate()
{
	LRItemSet set;
	set.completed = false;
	set.items.push_back(LRItem(Rule(_firstSymbol.get().symbolName, 0), SymbolRef(_nonTerminals[_firstSymbol.get().symbolName]), _nonTerminals[_firstSymbol.get().symbolName].production[0], _endSymbolRef, 0, true));
	toDoList.push_back(set);

	while (!toDoList.empty() || !incList.empty())
	{
		//Phase 1
		while (!toDoList.empty())
		{
			bool wasMerged = false;

			set = toDoList.front();
			toDoList.pop_front();

			GenerateClosureItems(set);

			int dSetIndex = 0;
			for (LRItemSet &dSet : doneList)
			{
				if (set.CanBeMergedIntoCores(dSet))
				{
					if (set.CanBeMergedIntoReduce(dSet))
					{
						MergeItemSets(set, dSet, !dSet.completed);

						if (comeFrom != -1)
						{
							for (LRItem & item : GetSetByUID(comeFrom).items)
							{
								if (item.action.type == Action::Shift && item.action.stateNo == set.stateUID)
								{
									item.action.stateNo = dSet.stateNo;
									item.action.stateUID = dSet.stateUID;
								}
							}
						}

						wasMerged = true;
						incList.push_back(dSet);
						break;
					}
					else
						LALR = false;
				}
				dSetIndex++;
			}

			if (!wasMerged)
			{
				set.stateNo = nStates++;
				doneList.push_back(set);
				incList.push_back(doneList.back());
			}
		}

		// Phase 2
		comeFrom = -1;
		int nextStateNo = nStates + 1;
		if (!incList.empty())
		{
			LRItemSet &incSet = incList.front();

			for (LRItem & item : incSet.items)
			{
				if (item.action.type != Action::Unset)
					continue;

				LRItemSet newSet;
				newSet.completed = false;
				std::string symbolName = item.RHS.get()[item.position].get().symbolName;
				for (LRItem & item2 : incSet.items)
				{
					if (item2.action.type != Action::Reduce && item2.RHS.get()[item2.position].get().symbolName == symbolName)
					{
						newSet.items.push_back(LRItem(item2.rule, item2.LHS, item2.RHS.get(), item2.lookahead, item2.position + 1, true));
						item2.action.type = Action::Shift;
						item2.action.lookahead = symbolName;
						item2.action.stateNo = newSet.stateUID;
						item2.action.stateUID = newSet.stateUID;
					}
				}

				toDoList.push_back(newSet);
				comeFrom = incSet.stateUID;
			}

			SetCompletedInDoneList(incSet.stateUID);
			incSet.completed = true;

			incList.pop_front();
		}
	}

	std::map<int, int> stateUIDToStateNoMap;
	for (LRItemSet set : doneList)
		stateUIDToStateNoMap[set.stateUID] = set.stateNo;

	std::map<Rule, int> ruleMap;
	ruleList.push_back(std::make_pair(std::ref<NonTerminal>((NonTerminal&)_firstSymbol.get()), 0));
	ruleMap[Rule(_firstSymbol.get().symbolName, 0)] = 0;
	for (auto &symbol : _nonTerminals)
	{
		if (symbol.second.symbolName != _firstSymbol.get().symbolName)
		{
			for (int i = 0; i < symbol.second.production.size(); i++)
			{
				ruleList.push_back(std::make_pair(std::ref(symbol.second), i));
				ruleMap[Rule(symbol.second.symbolName, i)] = ruleList.size() - 1;
			}
		}
		else
			ruleList[0] = std::make_pair(std::ref(symbol.second), 0);
	}

	for (LRItemSet &set : doneList)
	{
		for (LRItem &item : set.items)
		{
			if (item.action.type == Action::Shift)
			{
			
				item.action.stateNo = stateUIDToStateNoMap[item.action.stateUID];
				if (item.RHS.get()[item.position].get().type == Symbol::NonTerminal)
				{
					item.action.type = Action::Goto;
					item.action.lookahead = item.RHS.get()[item.position].get().symbolName;
				}
			}
			else if (item.action.type == Action::Reduce)
				item.action.stateNo = ruleMap[item.rule];
			else if (item.action.type == Action::Unset)
			{
				bool found = false;
				std::string shiftSymbol = item.RHS.get()[item.position].get().symbolName;
				for (LRItem &item2 : set.items)
				{
					if (item2.action.type != Action::Reduce && item2.RHS.get()[item2.position].get().symbolName == shiftSymbol && item2.action.type != Action::Unset)
					{
						item.action.type = item2.action.type;
						item.action.stateNo = item2.action.stateNo;
						item.action.lookahead = shiftSymbol;
						found = true;
						std::cout << "Found unset item \"" << item.GetCoreHash() <<"\" in set " << set.stateNo << ", setting action to (" << ((item.action.type == Action::Shift) ? "s" : "g") << "," << item.action.stateNo << "\n";
						break;
					}
				}
				if (!found)
					throw "Found unset action that cannot be resolved!";
			}
		}
	}
}