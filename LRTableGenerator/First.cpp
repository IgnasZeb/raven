#include "First.h"


void First::CalculateFirst(SymbolRef symbol)
{
	std::string &symbolName = symbol.get().symbolName;
	std::vector<SymbolRef> appendFirsts;
	Symbol outSymbol;
	if (FirstRule(symbol, outSymbol))
		_firsts[symbolName].push_back(outSymbol);
	if (SecondRule(symbol, appendFirsts))
		_firsts[symbolName].insert(_firsts[symbolName].end(), appendFirsts.begin(), appendFirsts.end());
	if (ThirdRule(symbol, appendFirsts))
		_firsts[symbolName].insert(_firsts[symbolName].end(), appendFirsts.begin(), appendFirsts.end());
}

bool First::FirstCalculated(SymbolRef symbolRef)
{
	return _firsts.find(symbolRef.get().symbolName) != _firsts.end();
}

bool First::FirstRule(SymbolRef symbol, Symbol &outSymbol)
{
	if (symbol.get().type == Symbol::Terminal)
	{
		outSymbol = symbol;
		return true;
	}
	return false;
}

bool First::SecondRule(SymbolRef symbol, std::vector<SymbolRef> &symbols)
{
	if (symbol.get().type == Symbol::Terminal)
		return false;

	std::vector<SymbolRef>().swap(symbols);
	for (const std::vector<SymbolRef>& symbolVec : ((NonTerminal&)symbol.get()).production)
	{
		if (symbolVec[0].get().type == Symbol::Terminal)
		{
			CalculateFirst(symbolVec[0]);
			symbols.push_back(symbolVec[0]);
		}
	}
	return symbols.size() != 0;
}

bool First::ThirdRule(SymbolRef symbol, std::vector<SymbolRef> &symbols)
{
	if (symbol.get().type == Symbol::Terminal)
		return false;
	
	std::vector<SymbolRef>().swap(symbols);
	for (const std::vector<SymbolRef>& symbolVec : ((NonTerminal&)symbol.get()).production)
	{
		if (symbolVec[0].get().type == Symbol::NonTerminal)
		{
			if (!FirstCalculated(symbolVec[0]) && symbol.get().symbolName != symbolVec[0].get().symbolName)
				CalculateFirst(symbolVec[0]);
			symbols.insert(symbols.end(), _firsts[symbolVec[0].get().symbolName].begin(), _firsts[symbolVec[0].get().symbolName].end());
		}
	}
	return symbols.size() != 0;
}

First::First(std::map<std::string, NonTerminal>& symbols, std::map<std::string, Terminal> &terminals) : _nonTerminals(symbols), _terminals(terminals)
{
}

First::~First()
{
}

std::map<std::string, std::vector<SymbolRef>>& First::CalculateFirsts()
{
	for (auto i = _nonTerminals.begin(); i != _nonTerminals.end(); i++)
	{
		if (!FirstCalculated(i->second))
			CalculateFirst(i->second);
	}
	for (auto i = _terminals.begin(); i != _terminals.end(); i++)
	{
		if (!_firsts[i->first].empty())
			_firsts[i->first].clear();
		_firsts[i->first].push_back(i->second);
	}
	return _firsts;
}
