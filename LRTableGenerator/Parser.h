#pragma once
#include <fstream>
#include <map>

#include "Terminal.h"
#include "NonTerminal.h"

class Parser
{
private:
	std::ifstream _reader;
public:
	std::map<std::string, Terminal> terminals;
	std::map<std::string, NonTerminal> nonTerminals;
	NonTerminal firstSymbol;

	Parser(const char * filePath);
	Parser(const wchar_t * filePath);

	void Parse();

	~Parser();
};

