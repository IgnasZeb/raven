#include "Parser.h"

#include <string>
#include <sstream>

Parser::Parser(const char * filePath) : _reader(filePath), firstSymbol("Undefined")
{
}

Parser::Parser(const wchar_t * filePath) : _reader(filePath), firstSymbol("Undefined")
{
}

void Parser::Parse()
{
	_reader.seekg(0, std::ios::beg);

	std::string line, symbolName, orClause, nonterminalName;
	std::stringstream orStream, symbolStream;
	while (std::getline(_reader, line))
	{
		int pos = line.find("::=");
		if (pos == std::string::npos)
			continue;

		nonterminalName = line.substr(0, pos - 1);
		line = line.substr(pos + 4);
		if (nonTerminals.find(nonterminalName) == nonTerminals.end())
		{
			nonTerminals[nonterminalName] = NonTerminal(nonterminalName);
		}
		NonTerminal & leftNonterminal = nonTerminals[nonterminalName];

		if (firstSymbol.symbolName == "Undefined")
			firstSymbol.symbolName = leftNonterminal.symbolName;

		_ASSERT(leftNonterminal.production.size() == 0);

		orStream << line;
		while (std::getline(orStream, orClause, '|'))
		{

			leftNonterminal.production.push_back(std::vector<SymbolRef>());
			symbolStream << orClause;
			while (std::getline(symbolStream, symbolName, ' '))
			{
				if (symbolName == "")
					continue;
				if (symbolName[0] == '<')
				{
					if (nonTerminals.find(symbolName) == nonTerminals.end())
						nonTerminals[symbolName] = NonTerminal(symbolName);
					leftNonterminal.production.back().push_back(nonTerminals[symbolName]);
				}
				else
				{
					if (terminals.find(symbolName) == terminals.end())
						terminals[symbolName] = Terminal(symbolName);
					leftNonterminal.production.back().push_back(terminals[symbolName]);
				}
			}
			symbolStream.clear();
		}
		orStream.clear();
	}
}

Parser::~Parser()
{
}
