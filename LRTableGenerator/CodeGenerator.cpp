#include "CodeGenerator.h"
#include <algorithm>

namespace CodeGenerator
{

	void GenerateCpp(std::string filePath, std::vector<std::pair<std::reference_wrapper<NonTerminal>, int>> RHS,
		std::map<int, std::map<std::string, Action>> actionTable,
		std::map<int, std::map<std::string, int>> gotoTable)
	{
		//std::ofstream file("C:\\Users\\ignas\\Documents\\Visual Studio 2017\\Projects\\Raven\\Raven\\Syntax Analyser\\LRTable.cpp");
		std::ofstream file(filePath);

		file << "#include \"LRTable.h\"\n";

		for (auto & r : RHS)
		{
			if (r.second == 0 && r.first.get().symbolName != "<if_else_statement>")
			{
				std::string SymbolName = r.first.get().symbolName;

				SymbolName.erase(SymbolName.begin());
				SymbolName.erase(SymbolName.end()-1);

				bool makeUpper = true;

				std::for_each(SymbolName.begin(), SymbolName.end(), [&makeUpper](char &c) {
					if (makeUpper)
					{
						c = toupper(c);
						makeUpper = false;
					}
					else if (c == '_')
						makeUpper = true;
				});
				SymbolName.erase(std::remove(SymbolName.begin(), SymbolName.end(), '_'), SymbolName.end());
				file << "#include \"Syntax Tree/" << SymbolName << ".h\"\n";
			}
		}

		file << "\nnamespace LR\n";
		file << "{\n";

		file << "template<class T>\nNode * InstantiateNode(std::list<Token> tkns, std::list<Node*> nodes)\n{\n\treturn new T(tkns, nodes);\n}\n\n";

		file << "std::vector<Rule> RuleTable{";
		for (int i = 0; i < RHS.size(); i++)
		{
			std::vector<SymbolRef> &prod = RHS[i].first.get().production[RHS[i].second];
			int terminals = 0, nonterminals = 0;

			for (const SymbolRef & symbol : prod)
			{
				if (symbol.get().type == Symbol::Terminal)
					terminals++;
				else
					nonterminals++;
			}

			file << "Rule(" << terminals << ", " << nonterminals << ", \"" << RHS[i].first.get().symbolName << ((i == RHS.size() - 1) ? "\")};\n\n" : "\"), ");
		}

		file << "std::map<int, std::map<TokenType, Action>> ActionTable {";
		bool first = true;
		for (const std::pair<int, std::map<std::string, Action>> &state : actionTable)
		{
			if (!first)
				file << ",\n";

			file << "{" << state.first << ", {";

			first = true;
			for (const std::pair < std::string, Action > &action : state.second)
			{
				if (action.second.type != Action::Goto)
				{
					if (!first)
						file << ", ";
					first = false;

					file << "{ TokenType::" << ((action.first == "EOF") ? "_EOF" : action.first) << ", Action(Action::" << ((action.second.type == Action::Shift) ? "Shift, " : "Reduce, ") << action.second.stateNo << ")}";
				}
			}

			file << "}}";
		}
		file << " };\n\n";


		file << "std::map<int, std::map<std::string, int>> GotoTable {";
		first = true;
		for (const std::pair<int, std::map<std::string, int>> &state : gotoTable)
		{
			if (!first)
				file << ",\n";

			file << "{" << state.first << ", {";

			first = true;
			for (const std::pair <std::string, int> &action : state.second)
			{
				if (!first)
					file << ", ";
				first = false;

				file << "{\"" << action.first << "\", " << action.second << "}";
			}

			file << "}}";
		}
		file << " };\n\n";

		file << "std::vector<std::function<Node*(std::list<Token>, std::list<Node*>)>> ReduceActionTable {";
		for (int i = 0; i < RHS.size(); i++)
		{
			std::string SymbolName = RHS[i].first.get().symbolName;
			if (SymbolName == "<if_else_statement>")
				SymbolName = "IfStatement";
			else
			{
				SymbolName.erase(SymbolName.begin());
				SymbolName.erase(SymbolName.end() - 1);

				bool makeUpper = true;

				std::for_each(SymbolName.begin(), SymbolName.end(), [&makeUpper](char &c) {
					if (makeUpper)
					{
						c = toupper(c);
						makeUpper = false;
					}
					else if (c == '_')
						makeUpper = true;
				});
				SymbolName.erase(std::remove(SymbolName.begin(), SymbolName.end(), '_'), SymbolName.end());
			}

			if (i != 0)
				file << ", ";

			file << "InstantiateNode<" << SymbolName << ">";
		}
		file << "};\n}";


		file.close();
	}

	void GenerateHeader(std::string filePath)
	{
		std::ofstream file(filePath);

		file << "#pragma once\n\n#include <map>\n#include <vector>\n#include <string>\n#include <functional>\n#include \"../Token.h\"\n#include \"Syntax Tree/Node.h\"\n\nstruct Action\n{\n\tenum ActionType { Error, Shift, Reduce, Goto };\n\tActionType Type;\n\tint State;\n\tAction() : State(-1), Type(Error) {};\n\tAction(ActionType a, int s) : Type(a), State(s) {}\n};\n\nnamespace LR\n{\n\tstruct Rule\n\t{\n\t\tint TerminalCount;\n\t\tint NonTerminalCount;\n\t\tstd::string LHSName;\n\t\tRule() {};\n\t\tRule(int tc, int nc, std::string lhs) : TerminalCount(tc), NonTerminalCount(nc), LHSName(lhs) {}\n\t};\n\n\textern std::vector<Rule> RuleTable;\n\textern std::map<int, std::map<TokenType, Action>> ActionTable;\n\textern std::map<int, std::map<std::string, int>> GotoTable;\n\textern std::vector<std::function<Node*(std::list<Token>, std::list<Node*>)>> ReduceActionTable;\n}";

		file.close();
	}
}