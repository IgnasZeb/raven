#pragma once
#include <list>
#include <map>
#include "LRItemSet.h"
#include "NonTerminal.h"

class TableGenerator
{
private:
	std::list<std::reference_wrapper<LRItemSet>> incList;
	std::list<LRItemSet> toDoList;
	int comeFrom = -1;
	int nStates = 0;

	std::map<std::string, std::vector<SymbolRef>> & _firsts;
	std::map < std::string, NonTerminal> & _nonTerminals;
	SymbolRef _firstSymbol;
	SymbolRef _endSymbolRef;
	Symbol _endSymbol;

	void GenerateClosureItems(LRItemSet & set);
	void MergeItemSets(LRItemSet &merge, LRItemSet &into, bool mergeNonkernels);
	void SetCompletedInDoneList(int uid);
	LRItemSet &GetSetByUID(int uid);
public:
	std::vector<std::pair<std::reference_wrapper<NonTerminal>, int>> ruleList;
	std::list<LRItemSet> doneList;
	bool LALR = true;

	TableGenerator(std::map<std::string, std::vector<SymbolRef>> & firsts, std::map < std::string, NonTerminal> & nonTerminals, SymbolRef firstSymbol);
	~TableGenerator();

	void Generate();
};

