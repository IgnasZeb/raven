#pragma once

#include <vector>

namespace CMDUtils
{
	typedef std::vector<std::pair<std::string, std::reference_wrapper<bool>>> SwitchArgs;
	typedef std::vector<std::pair<std::string, std::reference_wrapper<std::string>>> ValueArgs;

	int ParseArguments(int argc, char **argv, SwitchArgs booleanArgs, ValueArgs stringArgs, bool notFoundException = true);
}